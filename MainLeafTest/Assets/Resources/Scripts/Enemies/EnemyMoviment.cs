using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMoviment : MonoBehaviour
{
    public GameObject Point1;

    public GameObject Point2;

    public GameObject Point3;

    public GameObject Point4;

    public GameObject CurrentPoint;

    NavMeshAgent agent;

    Animator animator;

    void Start()
    {
        CurrentPoint = Point1;
        Patrol();
        animator = GetComponent<Animator>();
        animator.SetBool("Run", true);
    }

    public void Patrol()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(CurrentPoint.transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Point1")
        {
            CurrentPoint = Point2;
            //StartCoroutine(PauseBetweenPoints(2));
        }
        else if(other.tag == "Point2")
        {
            CurrentPoint = Point3;
            //StartCoroutine(PauseBetweenPoints(3));
        }
        else if(other.tag == "Point3")
        {
            CurrentPoint = Point4;
            //StartCoroutine(PauseBetweenPoints(4));
        }
        else if(other.tag == "Point4")
        {
            CurrentPoint = Point1;
            //StartCoroutine(PauseBetweenPoints(1));
        }
        Patrol();
    }

    IEnumerator PauseBetweenPoints(int numberPoint)
    {
        animator.SetBool("Run", false);
        yield return new WaitForSeconds(2f);
        animator.SetBool("Run", true);
        if (numberPoint == 1)
        {
            CurrentPoint = Point2;
        }
        else if(numberPoint == 2)
        {
            CurrentPoint = Point3;
        }
        else if(numberPoint == 3)
        {
            CurrentPoint = Point4;
        }
        else if(numberPoint == 4)
        {
            CurrentPoint = Point1;
        }
        Patrol();
    }

}
