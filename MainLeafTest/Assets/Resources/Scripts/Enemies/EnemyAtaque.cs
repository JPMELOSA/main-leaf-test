using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAtaque : MonoBehaviour
{
    [SerializeField] private GameObject yougGotCaught;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            AudioManager.AU.PlaySFX("LOSEBIP");
            yougGotCaught.SetActive(true);
            StartCoroutine(StopGame());
        }
    }

    IEnumerator StopGame()
    {
        yield return new WaitForSeconds(0.4f);
        Debug.Log("vo�� foi Pego!!!");
        MainMenu.MMC.activeMenuGameOver();
        yield return new WaitForSeconds(1f);
        Time.timeScale = 0;
    }
}
