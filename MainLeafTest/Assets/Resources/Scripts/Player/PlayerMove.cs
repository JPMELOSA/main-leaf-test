using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
	[Header("Player")]
	[Tooltip("Qu�o r�pido o personagem se vira para a dire��o do movimento do rosto")]
	[Range(0.0f, 0.3f)]
	public float RotationSmoothTime = 0.12f;
	[Tooltip("Acelera��o e desacelera��o")]
	public float SpeedChangeRate = 10.0f;

	// player
	private float _speed;
	private float _animationBlend;
	private float _targetRotation = 0.0f;
	private float _rotationVelocity;
	private float _verticalVelocity;
	private CharacterController _controller;
	private GameObject _mainCamera;

	[Space(10)]
	[Tooltip("A altura que o jogador pode pular")]
	public float JumpHeight = 1.2f;
	[Tooltip("O personagem usa seu pr�prio valor de gravidade. O padr�o do mecanismo � -9.81f")]
	public float Gravity = -15.0f;

    private void Awake()
    {
		if (_mainCamera == null)
		{
			_mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		}
	}

    private void Start()
    {
		_controller = GetComponent<CharacterController>();
	}
    void Update()
	{
		Move();
	}

	private void Move()
	{
		// define a velocidade alvo com base na velocidade de movimento, velocidade de sprint e se sprint for pressionado
		float targetSpeed = 5;

		// uma acelera��o e desacelera��o simplistas projetadas para serem f�ceis de remover, substituir ou iterar

		// nota: o operador == do Vector2 usa aproxima��o, portanto n�o � propenso a erros de ponto flutuante e � mais barato que magnitude
		// se n�o houver entrada, defina a velocidade alvo para 0
		if ((Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0))
		{
			targetSpeed = 0.0f;
		}

		// uma refer�ncia � velocidade horizontal atual do jogador
		float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

		float speedOffset = 0.1f;
		float inputMagnitude = 1f; // Ver isso aqui

		// acelera ou desacelera at� a velocidade alvo
		if (currentHorizontalSpeed < targetSpeed - speedOffset || currentHorizontalSpeed > targetSpeed + speedOffset)
		{
			// cria um resultado curvo ao inv�s de um linear dando uma mudan�a de velocidade mais org�nica
			// note que T em Lerp est� preso, ent�o n�o precisamos fixar nossa velocidade
			_speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude, Time.deltaTime * SpeedChangeRate);

			// velocidade arredondada para 3 casas decimais
			_speed = Mathf.Round(_speed * 1000f) / 1000f;
		}
		else
		{
			_speed = targetSpeed;
		}
		_animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, Time.deltaTime * SpeedChangeRate);

		// normaliza a dire��o de entrada
		Vector3 inputDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")).normalized;

		// nota: o operador != do Vector2 usa aproxima��o, portanto n�o � propenso a erros de ponto flutuante e � mais barato que magnitude
		// se houver um movimento de entrada gira o jogador quando o jogador est� se movendo
		if ((Input.GetAxis("Horizontal") != 0 && Input.GetAxis("Vertical") != 0))
		{
			_targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + _mainCamera.transform.eulerAngles.y;
			float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity, RotationSmoothTime);

			// gira para a dire��o de entrada de face em rela��o � posi��o da c�mera
			transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
		}


		Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;

		// move o jogador
		_controller.Move(targetDirection.normalized * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

		// atualiza o animador se estiver usando o personagem
	}
}
