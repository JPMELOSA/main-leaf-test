using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullingBox : MonoBehaviour
{
    #region PULL PUSH
    public static PullingBox PU;

    private void Awake()
    {

        if (PullingBox.PU == null)
        {
            PullingBox.PU = this;
        }
        else
        {
            if (PullingBox.PU != this)
                Destroy(this.gameObject);
        }

    }

    #endregion PULL PUSH

    public bool pull;
    public bool push;

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "BOX")
        {
            if(Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.Mouse0))
            {
                pull = true;
                push = false;
            }
            else
            {
                pull = false;
            }
            //
            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.Mouse0))
            {
                push = true;
            }
            else
            {
                push = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "BOX")
        {
            pull = false;
            push = false;
        }
    }
}
