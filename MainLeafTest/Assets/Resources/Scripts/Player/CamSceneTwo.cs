using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSceneTwo : MonoBehaviour
{
    [Range(1, 100)]
    [SerializeField] private float smoothFactor;


    [Header("Main Cam")]

    [SerializeField] Camera cam;

    [Header("Player")]

    [SerializeField] GameObject Player;


    void Start()
    {
        smoothFactor = 5;
    }
    private void FixedUpdate()
    {
        follow();
    }

    void follow()
    {
        if(Player.transform.position.x <= -25.70653)
        {
            Debug.Log("Parte Final!!");
        }
        else
        {
            Vector3 targetPosition = new Vector3(Player.transform.position.x, cam.transform.position.y, cam.transform.position.z);
            Vector3 smoothedPosition = Vector3.Lerp(cam.transform.position, targetPosition, smoothFactor * Time.fixedDeltaTime);
            cam.transform.position = smoothedPosition;
        }
    }
}
