using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Controllers
    private Vector3 PlayerMovementInput;
    [Header("Player")]
    [SerializeField] private Rigidbody PlayerBody;
    [SerializeField] private GameObject PlayerBodyRot;
    [Space(15)]

    [Header("Velocidade da Rota��o")]
    [Range(1, 10)]
    [SerializeField] private float Speed;

    [Header("Velocidade da Rota��o")]
    [Range(200, 300)]
    [SerializeField] private float JumpForce;

    [Header("Velocidade da Rota��o")]
    [Range(1, 1000)]
    [SerializeField] private float rotSpeed = 900;
    private float rot;

    Animator animator;

    [Header("P�s")]
    [SerializeField] private Transform Feet;
    private bool isGround;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovementInput = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Animations();
        MovementPlayer();
    }

    private void MovementPlayer()
    {
        Vector3 MoveVector = transform.TransformDirection(PlayerMovementInput) * Speed;
        PlayerBody.velocity = new Vector3(MoveVector.x, PlayerBody.velocity.y, MoveVector.z);

        rot += Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
        PlayerBodyRot.transform.eulerAngles = new Vector3(0, rot, 0);
        

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGround)
            {
                PlayerBody.AddForce(Vector3.up * JumpForce, ForceMode.Impulse);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Ground")
        {
            isGround = true;
        }
        if(other.tag == "BOX")
        {
            isGround = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Ground")
        {
            isGround = false;
        }
        if (other.tag == "BOX")
        {
            isGround = false;
        }
    }

    #region Animation

    private void Animations()
    {
        Idle();
        // Caminhar
        if (isGround)
        {
            // Caminhar para Frente
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            {
                //Speed = 2f;
                Run();
                //Turn(0);
            }
            if (Input.GetKeyUp(KeyCode.W))
            {
                Idle();
            }
        }
        else
        {
            Fall();
        }
        // ------------------------------
        //Caminhar para tr�s
        if (Input.GetKey(KeyCode.S))
        {
            //Speed = 1f;
            WalkBack();
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            Idle();
        }
        // ------------------------------
        // -- Puxar e Arrastar
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.Mouse0))
        {
            Push();
        }
        //
        if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.Mouse0))
        {
            Pull();
        }
        if (Input.GetKeyUp(KeyCode.S) && Input.GetKeyUp(KeyCode.Mouse0))
        {
        }
        //
        // JUMP
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
        // ------------------------------
        // Agachar
        if (Input.GetKey(KeyCode.R))
        {
            Crawl();
        }

    }


    IEnumerator endJump()
    {
        yield return new WaitForSeconds(2f);
        animator.SetFloat("Jump", 0);
        animator.SetFloat("JumpLeg", 0);
        animator.SetBool("OnGround", true);
        animator.SetFloat("Forward", 0);
    }

    private void Idle()
    {
        //Turn(0);
        animator.SetBool("Run", false);
        animator.SetBool("Push", false);
        animator.SetBool("Pull", false);
        animator.SetBool("Crawl", false);
        animator.SetBool("Jump", false);
        animator.SetBool("WalkBack", false);
        animator.SetBool("Fall", false);
        animator.SetBool("Run", false);
    }

    private void Run()
    {
        animator.SetBool("Run", true);
        animator.SetBool("Push", false);
        animator.SetBool("Pull", false);
        animator.SetBool("Crawl", false);
        animator.SetBool("Jump", false);
        animator.SetBool("WalkBack", false);
        animator.SetBool("Fall", false);
    }

    private void WalkBack()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Push", false);
        animator.SetBool("Pull", false);
        animator.SetBool("Crawl", false);
        animator.SetBool("Jump", false);
        animator.SetBool("WalkBack", true);
        animator.SetBool("Fall", false);
    }

    private void Jump()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Push", false);
        animator.SetBool("Pull", false);
        animator.SetBool("Crawl", false);
        animator.SetBool("Jump", true);
        animator.SetBool("WalkBack", false);
        animator.SetBool("Fall", false);
    }

    private void Pull()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Push", false);
        animator.SetBool("Pull", true);
        animator.SetBool("Crawl", false);
        animator.SetBool("Jump", false);
        animator.SetBool("WalkBack", false);
        animator.SetBool("Fall", false);
    }

    private void Push()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Push", true);
        animator.SetBool("Pull", false);
        animator.SetBool("Crawl", false);
        animator.SetBool("Jump", false);
        animator.SetBool("WalkBack", false);
        animator.SetBool("Fall", false);
    }

    private void Crawl()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Push", false);
        animator.SetBool("Pull", false);
        animator.SetBool("Crawl", true);
        animator.SetBool("Jump", false);
        animator.SetBool("WalkBack", false);
        animator.SetBool("Fall", false);
    }

    private void Fall()
    {
        animator.SetBool("Run", false);
        animator.SetBool("Push", false);
        animator.SetBool("Pull", false);
        animator.SetBool("Crawl", false);
        animator.SetBool("Jump", false);
        animator.SetBool("WalkBack", false);
        animator.SetBool("Fall", true);
    }


    #endregion Animation
}
