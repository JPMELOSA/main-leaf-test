using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSceneOne : MonoBehaviour
{
    [SerializeField] private float mouseSensivity = 6;
    [SerializeField] private Transform _target;
    [SerializeField] private float distanceFromTarget = 5;
    [SerializeField] private Vector2 pitchMinMax = new Vector2(20, 55);

    [SerializeField] private float rotationSmoothTime = 0.12f;
    [SerializeField] private Vector3 rotationSmoothVelocity;
    [SerializeField] private Vector3 currentRotation;

    [SerializeField] private float yaw;
    [SerializeField] private float pitch;

    void LateUpdate()
    {
        cameraMovement();
    }

    private void cameraMovement()
    {
        yaw += Input.GetAxis("Mouse X") * mouseSensivity;
        pitch -= Input.GetAxis("Mouse Y") * mouseSensivity;
        pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);

        currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);

        transform.eulerAngles = currentRotation;
        transform.position = _target.position - transform.forward * distanceFromTarget;
    }
}
