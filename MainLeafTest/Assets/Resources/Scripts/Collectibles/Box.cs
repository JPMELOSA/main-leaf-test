using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    [SerializeField] private GameObject Player;

    public PullingBox PlayerPulling;

    [Range(1, 100)]
    [SerializeField] private float smoothFactor;

    [Range(1, 5)]
    [SerializeField] private float distance;

    public bool boxBeingPulled;

    Vector3 DifferenceBetweenLocations;

    [SerializeField] Rigidbody rigidbody;

    RigidbodyConstraints constraints;
    void Start()
    {
        PlayerPulling = GetComponent<PullingBox>();
        rigidbody = GetComponent<Rigidbody>();
        blockBox(true);
    }

    // Update is called once per frame
    void Update()
    {
        DifferenceBetweenLocations = new Vector3(transform.position.x - Player.transform.position.x, transform.position.y - Player.transform.position.y, transform.position.z - Player.transform.position.z);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            UIManager.UI.changeText("PULL");
            if (PullingBox.PU.pull)
            {
                blockBox(false);
                //Vector3 targetPosition = new Vector3(Player.transform.position.x + distance, transform.position.y, Player.transform.position.z + distance);
                Vector3 targetPosition = new Vector3(Player.transform.position.x + DifferenceBetweenLocations.x, transform.position.y, Player.transform.position.z + DifferenceBetweenLocations.z);
                Vector3 smoothedPosition = Vector3.Lerp(transform.position, targetPosition, smoothFactor * Time.fixedDeltaTime);
                transform.position = smoothedPosition;
            }
            else
            {
                blockBox(true);
            }
            if (PullingBox.PU.push)
            {
                blockBox(false);
            }
            else
            {
                blockBox(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            UIManager.UI.changeText("");
            boxBeingPulled = false;
            Debug.Log("Caixa Sendo Puxada: " + boxBeingPulled);
        }
    }

    public void blockBox(bool block)
    {
        if (block)
        {
            rigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }
        else
        {
            rigidbody.constraints = RigidbodyConstraints.None;
            // Positions
            rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }
    }
}
