using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystals : MonoBehaviour
{
    public enum Cor
    { 
        Azul,
        Vermelho,
        Verde
    }

    public Cor cor;

    private void Start()
    {
        CrytalColor();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //
            if(cor == Cor.Azul)
            {
                ScoreManager.SM.GetPoints(10);
                AudioManager.AU.PlaySFX("WINBIP");
                Destroy(this.gameObject);
            }
            else if(cor == Cor.Verde)
            {
                ScoreManager.SM.GetPoints(20);
                AudioManager.AU.PlaySFX("WINBIP");
                Destroy(this.gameObject);
            }
            else if (cor == Cor.Vermelho)
            {
                ScoreManager.SM.GetPoints(30);
                AudioManager.AU.PlaySFX("WINBIP");
                Destroy(this.gameObject);
            }
            else
            {
                Debug.LogError("!Erro Cor");
            }
        }
    }

    private void CrytalColor()
    {
        if (cor == Cor.Azul)
        {
            GetComponent<Renderer>().material.color = Color.blue;
        }
        else if (cor == Cor.Verde)
        {
            GetComponent<Renderer>().material.color = Color.green;
        }
        else if (cor == Cor.Vermelho)
        {
            GetComponent<Renderer>().material.color = Color.red;
        }
    }

}