using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    #region STATIC SCORE MANAGER CONTROLLER
    public static ScoreManager SM;

    private void Awake()
    {

        if (ScoreManager.SM == null)
        {
            ScoreManager.SM = this;
        }
        else
        {
            if (ScoreManager.SM != this)
                Destroy(this.gameObject);
        }

    }
    #endregion STATIC SCORE MANAGER CONTROLLER

    private int Score;

    public void GetPoints(int value) // Ver nome dessa fun��o
    {
        Score += value;
        UpdateScoreUI();
    }

    private void UpdateScoreUI()
    {
        UIManager.UI.Score.text = Score.ToString();
    }
}
