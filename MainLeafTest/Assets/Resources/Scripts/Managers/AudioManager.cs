using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region STATIC AUDIO MANAGER
    public static AudioManager AU;

    private void OnEnable()
    {
        if (AudioManager.AU == null)
        {
            AudioManager.AU = this;
        }
        else
        {
            if (AudioManager.AU != this)
                Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }


    #endregion STATIC AUDIO MANAGER

    [Header("AUDIO SOURCES")]
    public AudioSource music;
    public AudioSource sfx;

    [Header("MUSICS")]
    public AudioClip mainTheme;

    [Header("SFX")]
    public AudioClip mouseClick;
    public AudioClip winBip;
    public AudioClip loseBip;

    private void Start()
    {
        PlayMusic("MAINMENU");
    }

    public void PlayMusic(string musicType)
    {
        switch (musicType)
        {
            case "MAINMENU":
                {
                    music.clip = mainTheme;
                    music.Play();
                    break;

                }
            case "STOP":
                {
                    music.Stop();
                    break;
                }
        }
    }

    public void PlaySFX(string SFXType)
    {
        switch (SFXType)
        {
            case "MOUSECLICK":
                {
                    sfx.PlayOneShot(mouseClick, 1);
                    break;
                }
            case "WINBIP":
                {
                    sfx.PlayOneShot(winBip, 1);
                    break;
                }
            case "LOSEBIP":
                {
                    sfx.PlayOneShot(loseBip, 1);
                    break;
                }
        }
    }
}
