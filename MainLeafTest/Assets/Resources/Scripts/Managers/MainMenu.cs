using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    #region STATIC MAIN MENU CONTROLLER
    public static MainMenu MMC;

    private void Awake()
    {

        if (MainMenu.MMC == null)
        {
            MainMenu.MMC = this;
        }
        else
        {
            if (MainMenu.MMC != this)
                Destroy(this.gameObject);
        }
    }
    #endregion STATIC MAIN MENU CONTROLLER

    [Header("Game Objects Menus")]
    [SerializeField] private GameObject MenuPause;

    [SerializeField] private GameObject MenuGameOver;

    void Start()
    {
        StartCoroutine(SplashScene());
    }

    // Update is called once per frame
    void Update()
    {
        CheckPause();
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("02-FirstPart");
        Time.timeScale = 1;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void PauseGame(bool isPause)
    {
        MenuPause.SetActive(isPause);
        if (isPause)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    private void CheckPause()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            PauseGame(true);
        }
    }

    public void activeMenuGameOver()
    {
        MenuGameOver.SetActive(true);
    }

    IEnumerator SplashScene()
    {
        if(SceneManager.GetActiveScene().name == "00-SplashScene")
        {
            yield return new WaitForSeconds(1.5f);
            SceneManager.LoadScene("01-MainMenu");
        }
    }
}
