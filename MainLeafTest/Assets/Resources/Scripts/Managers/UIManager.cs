using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    #region STATIC UI MANAGER CONTROLLER
    public static UIManager UI;

    private void Awake()
    {

        if (UIManager.UI == null)
        {
            UIManager.UI = this;
        }
        else
        {
            if (UIManager.UI != this)
                Destroy(this.gameObject);
        }

    }

    #endregion STATIC UI MANAGER CONTROLLER


    [Header("Score")]

    public TextMeshProUGUI Score;

    [Header("Score")]

    public TextMeshProUGUI FunctionName;

    // Update is called once per frame

    public void changeText(string Text)
    {
        FunctionName.text = Text;
    }
}
