using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject message;

    private void CheckScene()
    {
        if (SceneManager.GetActiveScene().name == "02-FirstPart")
        {
            message.SetActive(true);
            UIManager.UI.changeText("CRAWL");
            if (Input.GetKeyDown(KeyCode.R))
            {
                StartCoroutine(CrawlTime());
            }
        }
        if(SceneManager.GetActiveScene().name == "03-SecondPart")
        {
            SceneManager.LoadScene("04-VictoryScene");
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            CheckScene();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            message.SetActive(false);
            UIManager.UI.changeText("");
        }
    }

    IEnumerator CrawlTime()
    {
        Debug.Log("ABAIXOU");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("03-SecondPart");
        message.SetActive(false);
    }
}
