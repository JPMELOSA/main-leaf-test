﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct Dictionary_2_t6559C3595494ACAFACFBD50DF3795F25E9D5FEB1;
// System.Func`2<UnityEngine.ProBuilder.Edge,System.Boolean>
struct Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F;
// System.Func`2<UnityEngine.ProBuilder.EdgeLookup,System.Boolean>
struct Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10;
// System.Func`2<UnityEngine.ProBuilder.EdgeLookup,System.Int32>
struct Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274;
// System.Func`2<System.Int32,UnityEngine.ProBuilder.Edge>
struct Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA;
// System.Func`2<System.Int32,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941;
// System.Func`2<System.Int32,System.Object>
struct Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123;
// System.Func`2<System.Int32,UnityEngine.Vector3>
struct Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>
struct Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.ProBuilder.Edge>
struct Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Int32>
struct Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct Func_2_tB442E424B8C858730982BF0E6EEA478246930263;
// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.Vector3>
struct Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>
struct Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.ProBuilder.Edge>
struct Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Int32>
struct Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C;
// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.Vector3>
struct Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>
struct Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.ProBuilder.Edge>
struct Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Int32>
struct Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829;
// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.Vector3>
struct Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Func`2<System.Object,UnityEngine.ProBuilder.Edge>
struct Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF;
// System.Func`2<System.Object,UnityEngine.ProBuilder.EdgeLookup>
struct Func_2_t675406C4352EE58B2C120699148BCA7644B247F3;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C;
// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92;
// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436;
// System.Func`2<System.Object,UnityEngine.Vector3>
struct Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>
struct Func_2_t914F2382286698ABCF7602CD671A49AD142738A8;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.ProBuilder.Edge>
struct Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Int32>
struct Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct Func_2_t005E8EF5E159894D8474A3B2F511313000B11680;
// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.Vector3>
struct Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3;
// System.Func`2<UnityEngine.Vector2,System.Boolean>
struct Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A;
// System.Func`2<UnityEngine.Vector2,UnityEngine.ProBuilder.Edge>
struct Func_2_t88AB91BBB6E00DC11936250A0515012606223C66;
// System.Func`2<UnityEngine.Vector2,System.Int32>
struct Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522;
// System.Func`2<UnityEngine.Vector2,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40;
// System.Func`2<UnityEngine.Vector2,System.Object>
struct Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020;
// System.Func`2<UnityEngine.Vector2,UnityEngine.Vector3>
struct Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B;
// System.Func`2<UnityEngine.Vector3,System.Boolean>
struct Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269;
// System.Func`2<UnityEngine.Vector4,System.Boolean>
struct Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148;
// System.Func`2<UnityEngine.Vector4,System.Object>
struct Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071;
// System.Func`2<UnityEngine.Vector4,UnityEngine.Vector2>
struct Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>
struct Func_2_tF3B895913B44A5233F386097973299392788EA81;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.ProBuilder.Edge>
struct Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Int32>
struct Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC;
// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.Vector3>
struct Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967;
// System.Collections.Generic.IEnumerable`1<UnityEngine.ProBuilder.Edge>
struct IEnumerable_1_t3D63D0351B8845325728FC343CFF583F7E51C356;
// System.Collections.Generic.IEnumerable`1<UnityEngine.ProBuilder.EdgeLookup>
struct IEnumerable_1_t3AAFEF8E8B82A3F8B12780051D4CD76C142564F5;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t60929E1AA80B46746F987B99A4EBD004FD72D370;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerable_1_tE4BC3C9BAA1C964715F4617E85A281C9418BE362;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_tF9FFC7B635421ED6396ABF58E4F5831F13B2C61F;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_tDBC849B8248C833C53F1762E771EFC477EB8AF18;
// System.Collections.Generic.IEnumerator`1<UnityEngine.ProBuilder.Edge>
struct IEnumerator_1_tD565B737BF910DAAD6DFCEF13E9341B3287DC535;
// System.Collections.Generic.IEnumerator`1<UnityEngine.ProBuilder.EdgeLookup>
struct IEnumerator_1_t33B89D7D07A40F2BCC4E143C220A0CB7D15244C6;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t72AB4B40AF5290B386215B0BFADC8919D394DCAB;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerator_1_t4F2FFA112134F73C0BA23053330D506ACF942F3B;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2DC97C7D486BF9E077C2BC2E517E434F393AA76E;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_tE27018507FAEDD46DFF02203E407053F3F338BEA;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t9C426231952B863270D78D88F9DB5B4E9A16CC6A;
// System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>
struct Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46;
// System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.EdgeLookup>
struct Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F;
// System.Linq.Enumerable/Iterator`1<System.Int32>
struct Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379;
// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB;
// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279;
// System.Linq.Enumerable/Iterator`1<UnityEngine.Vector2>
struct Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF;
// System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>
struct Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF;
// System.Collections.Generic.List`1<UnityEngine.ProBuilder.EdgeLookup>
struct List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>
struct List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>
struct List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.ProBuilder.EdgeLookup,System.Int32>
struct WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Int32>
struct WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Object>
struct WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Int32>
struct WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Int32>
struct WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.Vector3>
struct WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Int32>
struct WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.EdgeLookup>
struct WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Int32>
struct WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Int32>
struct WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Int32>
struct WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Object>
struct WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,System.Object>
struct WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,UnityEngine.Vector2>
struct WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Int32>
struct WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.ProBuilder.EdgeLookup[]
struct EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11;
// UnityEngine.InputSystem.Utilities.NameAndParameters[]
struct NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83;
// UnityEngine.InputSystem.Utilities.NamedValue[]
struct NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.InputSystem.Utilities.Substring[]
struct SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue[]
struct JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Linq.Enumerable/Iterator`1<System.Int32>
struct Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	int32_t ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379, ___current_2)); }
	inline int32_t get_current_2() const { return ___current_2; }
	inline int32_t* get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(int32_t value)
	{
		___current_2 = value;
	}
};


// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	RuntimeObject * ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___current_2)); }
	inline RuntimeObject * get_current_2() const { return ___current_2; }
	inline RuntimeObject ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(RuntimeObject * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_2), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.ProBuilder.EdgeLookup>
struct List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2, ____items_1)); }
	inline EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD* get__items_1() const { return ____items_1; }
	inline EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2_StaticFields, ____emptyArray_5)); }
	inline EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD* get__emptyArray_5() const { return ____emptyArray_5; }
	inline EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(EdgeLookupU5BU5D_tD6FDC2FA0337DF4DA2E6F04D5D2CD21F193DCBAD* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.InternedString>
struct List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371, ____items_1)); }
	inline InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* get__items_1() const { return ____items_1; }
	inline InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371_StaticFields, ____emptyArray_5)); }
	inline InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* get__emptyArray_5() const { return ____emptyArray_5; }
	inline InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(InternedStringU5BU5D_t157EC3FD5EC6C17780128E7E48FC136DB6E27D11* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50, ____items_1)); }
	inline NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83* get__items_1() const { return ____items_1; }
	inline NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50_StaticFields, ____emptyArray_5)); }
	inline NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NameAndParametersU5BU5D_t8313AFC154803B78FB2182249118FA823500EE83* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A, ____items_1)); }
	inline NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* get__items_1() const { return ____items_1; }
	inline NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A_StaticFields, ____emptyArray_5)); }
	inline NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.Substring>
struct List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5, ____items_1)); }
	inline SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959* get__items_1() const { return ____items_1; }
	inline SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5_StaticFields, ____emptyArray_5)); }
	inline SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SubstringU5BU5D_t41EDDE81A914C220D8683DEF4868E06E997A8959* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____items_1)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A, ____items_1)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A_StaticFields, ____emptyArray_5)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E, ____items_1)); }
	inline JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882* get__items_1() const { return ____items_1; }
	inline JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E_StaticFields, ____emptyArray_5)); }
	inline JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882* get__emptyArray_5() const { return ____emptyArray_5; }
	inline JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(JsonValueU5BU5D_t061F6C2DEF60669EB98890ACD2FE0E36E6997882* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___list_0)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_list_0() const { return ___list_0; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8, ___m_Array_0)); }
	inline NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* get_m_Array_0() const { return ___m_Array_0; }
	inline NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NamedValueU5BU5D_t3BC95F20A3983C313F80AF3BD283FDB060EA959B* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Int32>
struct WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<System.Object>
struct WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.ProBuilder.Edge
struct Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D 
{
public:
	// System.Int32 UnityEngine.ProBuilder.Edge::a
	int32_t ___a_0;
	// System.Int32 UnityEngine.ProBuilder.Edge::b
	int32_t ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D, ___b_1)); }
	inline int32_t get_b_1() const { return ___b_1; }
	inline int32_t* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(int32_t value)
	{
		___b_1 = value;
	}
};

struct Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D_StaticFields
{
public:
	// UnityEngine.ProBuilder.Edge UnityEngine.ProBuilder.Edge::Empty
	Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  ___Empty_2;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D_StaticFields, ___Empty_2)); }
	inline Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  get_Empty_2() const { return ___Empty_2; }
	inline Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D * get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  value)
	{
		___Empty_2 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// UnityEngine.InputSystem.Utilities.Substring
struct Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.Substring::m_String
	String_t* ___m_String_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.Substring::m_Index
	int32_t ___m_Index_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.Substring::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_String_0() { return static_cast<int32_t>(offsetof(Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815, ___m_String_0)); }
	inline String_t* get_m_String_0() const { return ___m_String_0; }
	inline String_t** get_address_of_m_String_0() { return &___m_String_0; }
	inline void set_m_String_0(String_t* value)
	{
		___m_String_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_String_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.Substring
struct Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815_marshaled_pinvoke
{
	char* ___m_String_0;
	int32_t ___m_Index_1;
	int32_t ___m_Length_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.Substring
struct Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815_marshaled_com
{
	Il2CppChar* ___m_String_0;
	int32_t ___m_Index_1;
	int32_t ___m_Length_2;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>
struct Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3, ___list_0)); }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * get_list_0() const { return ___list_0; }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3, ___current_3)); }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  get_current_3() const { return ___current_3; }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>
struct Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE, ___list_0)); }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * get_list_0() const { return ___list_0; }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE, ___current_3)); }
	inline Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  get_current_3() const { return ___current_3; }
	inline Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_String_0), (void*)NULL);
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>
struct Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68, ___list_0)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_list_0() const { return ___list_0; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68, ___current_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_current_3() const { return ___current_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___current_3 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>
struct Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781, ___list_0)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_list_0() const { return ___list_0; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781, ___current_3)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_current_3() const { return ___current_3; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___current_3 = value;
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>
struct Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46, ___current_2)); }
	inline Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  get_current_2() const { return ___current_2; }
	inline Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  value)
	{
		___current_2 = value;
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB, ___current_2)); }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  get_current_2() const { return ___current_2; }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.Vector2>
struct Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF, ___current_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_current_2() const { return ___current_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___current_2 = value;
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>
struct Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF, ___current_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_current_2() const { return ___current_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___current_2 = value;
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Int32>
struct WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325, ___source_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_source_3() const { return ___source_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325, ___selector_5)); }
	inline Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325, ___enumerator_6)); }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Object>
struct WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4, ___source_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_source_3() const { return ___source_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4, ___selector_5)); }
	inline Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4, ___enumerator_6)); }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Int32>
struct WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6, ___selector_5)); }
	inline Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * get_selector_5() const { return ___selector_5; }
	inline Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.ProBuilder.EdgeLookup
struct EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A 
{
public:
	// UnityEngine.ProBuilder.Edge UnityEngine.ProBuilder.EdgeLookup::m_Local
	Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  ___m_Local_0;
	// UnityEngine.ProBuilder.Edge UnityEngine.ProBuilder.EdgeLookup::m_Common
	Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  ___m_Common_1;

public:
	inline static int32_t get_offset_of_m_Local_0() { return static_cast<int32_t>(offsetof(EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A, ___m_Local_0)); }
	inline Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  get_m_Local_0() const { return ___m_Local_0; }
	inline Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D * get_address_of_m_Local_0() { return &___m_Local_0; }
	inline void set_m_Local_0(Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  value)
	{
		___m_Local_0 = value;
	}

	inline static int32_t get_offset_of_m_Common_1() { return static_cast<int32_t>(offsetof(EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A, ___m_Common_1)); }
	inline Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  get_m_Common_1() const { return ___m_Common_1; }
	inline Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D * get_address_of_m_Common_1() { return &___m_Common_1; }
	inline void set_m_Common_1(Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  value)
	{
		___m_Common_1 = value;
	}
};


// UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NameAndParameters::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue> UnityEngine.InputSystem.Utilities.NameAndParameters::<parameters>k__BackingField
	ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8  ___U3CparametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76, ___U3CparametersU3Ek__BackingField_1)); }
	inline ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8  get_U3CparametersU3Ek__BackingField_1() const { return ___U3CparametersU3Ek__BackingField_1; }
	inline ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8 * get_address_of_U3CparametersU3Ek__BackingField_1() { return &___U3CparametersU3Ek__BackingField_1; }
	inline void set_U3CparametersU3Ek__BackingField_1(ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8  value)
	{
		___U3CparametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8  ___U3CparametersU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t851C200D7DC2D0506E36559F1D46E7A0F5E8A1F8  ___U3CparametersU3Ek__BackingField_1;
};

// System.TypeCode
struct TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D 
{
public:
	// UnityEngine.InputSystem.Utilities.Substring UnityEngine.InputSystem.Utilities.JsonParser/JsonString::text
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  ___text_0;
	// System.Boolean UnityEngine.InputSystem.Utilities.JsonParser/JsonString::hasEscapes
	bool ___hasEscapes_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D, ___text_0)); }
	inline Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  get_text_0() const { return ___text_0; }
	inline Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 * get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___text_0))->___m_String_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_hasEscapes_1() { return static_cast<int32_t>(offsetof(JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D, ___hasEscapes_1)); }
	inline bool get_hasEscapes_1() const { return ___hasEscapes_1; }
	inline bool* get_address_of_hasEscapes_1() { return &___hasEscapes_1; }
	inline void set_hasEscapes_1(bool value)
	{
		___hasEscapes_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D_marshaled_pinvoke
{
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815_marshaled_pinvoke ___text_0;
	int32_t ___hasEscapes_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonString
struct JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D_marshaled_com
{
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815_marshaled_com ___text_0;
	int32_t ___hasEscapes_1;
};

// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType
struct JsonValueType_tC1693041766D5BCCE3DA251FABD56F8BA8539170 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonValueType_tC1693041766D5BCCE3DA251FABD56F8BA8539170, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.ProBuilder.EdgeLookup>
struct Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455, ___list_0)); }
	inline List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * get_list_0() const { return ___list_0; }
	inline List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455, ___current_3)); }
	inline EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  get_current_3() const { return ___current_3; }
	inline EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  value)
	{
		___current_3 = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4, ___list_0)); }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * get_list_0() const { return ___list_0; }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4, ___current_3)); }
	inline NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  get_current_3() const { return ___current_3; }
	inline NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.EdgeLookup>
struct Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable/Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable/Iterator`1::current
	EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F, ___current_2)); }
	inline EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  get_current_2() const { return ___current_2; }
	inline EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  value)
	{
		___current_2 = value;
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.ProBuilder.Edge>
struct WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49, ___predicate_4)); }
	inline Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066, ___predicate_4)); }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.Vector2>
struct WhereEnumerableIterator_1_tA5C39F085357986D0D6ABAC117C73C8BAE320A2C  : public Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tA5C39F085357986D0D6ABAC117C73C8BAE320A2C, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tA5C39F085357986D0D6ABAC117C73C8BAE320A2C, ___predicate_4)); }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tA5C39F085357986D0D6ABAC117C73C8BAE320A2C, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.Vector3>
struct WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8, ___predicate_4)); }
	inline Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C, ___source_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_source_3() const { return ___source_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C, ___selector_5)); }
	inline Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C, ___enumerator_6)); }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4, ___source_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_source_3() const { return ___source_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4, ___selector_5)); }
	inline Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4, ___enumerator_6)); }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5, ___source_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_source_3() const { return ___source_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5, ___selector_5)); }
	inline Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5, ___enumerator_6)); }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19, ___source_3)); }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * get_source_3() const { return ___source_3; }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19, ___predicate_4)); }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19, ___selector_5)); }
	inline Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE * get_selector_5() const { return ___selector_5; }
	inline Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19, ___enumerator_6)); }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Int32>
struct WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C, ___source_3)); }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * get_source_3() const { return ___source_3; }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C, ___predicate_4)); }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C, ___selector_5)); }
	inline Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C, ___enumerator_6)); }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144, ___source_3)); }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * get_source_3() const { return ___source_3; }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144, ___predicate_4)); }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144, ___selector_5)); }
	inline Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC * get_selector_5() const { return ___selector_5; }
	inline Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144, ___enumerator_6)); }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tB442E424B8C858730982BF0E6EEA478246930263 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960, ___source_3)); }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * get_source_3() const { return ___source_3; }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960, ___predicate_4)); }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960, ___selector_5)); }
	inline Func_2_tB442E424B8C858730982BF0E6EEA478246930263 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB442E424B8C858730982BF0E6EEA478246930263 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB442E424B8C858730982BF0E6EEA478246930263 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960, ___enumerator_6)); }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D, ___source_3)); }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * get_source_3() const { return ___source_3; }
	inline List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D, ___predicate_4)); }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D, ___selector_5)); }
	inline Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D, ___enumerator_6)); }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8, ___selector_5)); }
	inline Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF * get_selector_5() const { return ___selector_5; }
	inline Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD, ___selector_5)); }
	inline Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1, ___selector_5)); }
	inline Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363, ___source_3)); }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * get_source_3() const { return ___source_3; }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363, ___predicate_4)); }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363, ___selector_5)); }
	inline Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363, ___enumerator_6)); }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Int32>
struct WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6, ___source_3)); }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * get_source_3() const { return ___source_3; }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6, ___predicate_4)); }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6, ___selector_5)); }
	inline Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6, ___enumerator_6)); }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127, ___source_3)); }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * get_source_3() const { return ___source_3; }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127, ___predicate_4)); }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127, ___selector_5)); }
	inline Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127, ___enumerator_6)); }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F, ___source_3)); }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * get_source_3() const { return ___source_3; }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F, ___predicate_4)); }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F, ___selector_5)); }
	inline Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F, ___enumerator_6)); }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529, ___source_3)); }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * get_source_3() const { return ___source_3; }
	inline List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529, ___predicate_4)); }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529, ___selector_5)); }
	inline Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529, ___enumerator_6)); }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___m_String_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F, ___source_3)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_source_3() const { return ___source_3; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F, ___predicate_4)); }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F, ___selector_5)); }
	inline Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F, ___enumerator_6)); }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Int32>
struct WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D, ___source_3)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_source_3() const { return ___source_3; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D, ___predicate_4)); }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D, ___selector_5)); }
	inline Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D, ___enumerator_6)); }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07, ___source_3)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_source_3() const { return ___source_3; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07, ___predicate_4)); }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07, ___selector_5)); }
	inline Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07, ___enumerator_6)); }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Object>
struct WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5, ___source_3)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_source_3() const { return ___source_3; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5, ___predicate_4)); }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5, ___selector_5)); }
	inline Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5, ___enumerator_6)); }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838, ___source_3)); }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * get_source_3() const { return ___source_3; }
	inline List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838, ___predicate_4)); }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838, ___selector_5)); }
	inline Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838, ___enumerator_6)); }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,System.Object>
struct WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645, ___source_3)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_source_3() const { return ___source_3; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645, ___predicate_4)); }
	inline Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645, ___selector_5)); }
	inline Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645, ___enumerator_6)); }
	inline Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,UnityEngine.Vector2>
struct WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C  : public Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C, ___source_3)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_source_3() const { return ___source_3; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C, ___predicate_4)); }
	inline Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C, ___selector_5)); }
	inline Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C, ___enumerator_6)); }
	inline Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.TypeCode UnityEngine.InputSystem.Utilities.PrimitiveValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			// System.Boolean UnityEngine.InputSystem.Utilities.PrimitiveValue::m_BoolValue
			bool ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			bool ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			// System.Char UnityEngine.InputSystem.Utilities.PrimitiveValue::m_CharValue
			Il2CppChar ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			Il2CppChar ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			// System.Byte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ByteValue
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			// System.SByte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_SByteValue
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			// System.Int16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ShortValue
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			// System.UInt16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UShortValue
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			// System.Int32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_IntValue
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			// System.UInt32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UIntValue
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			// System.Int64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_LongValue
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			// System.UInt64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ULongValue
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			// System.Single UnityEngine.InputSystem.Utilities.PrimitiveValue::m_FloatValue
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			// System.Double UnityEngine.InputSystem.Utilities.PrimitiveValue::m_DoubleValue
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BoolValue_1() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_BoolValue_1)); }
	inline bool get_m_BoolValue_1() const { return ___m_BoolValue_1; }
	inline bool* get_address_of_m_BoolValue_1() { return &___m_BoolValue_1; }
	inline void set_m_BoolValue_1(bool value)
	{
		___m_BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_m_CharValue_2() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_CharValue_2)); }
	inline Il2CppChar get_m_CharValue_2() const { return ___m_CharValue_2; }
	inline Il2CppChar* get_address_of_m_CharValue_2() { return &___m_CharValue_2; }
	inline void set_m_CharValue_2(Il2CppChar value)
	{
		___m_CharValue_2 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_3() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ByteValue_3)); }
	inline uint8_t get_m_ByteValue_3() const { return ___m_ByteValue_3; }
	inline uint8_t* get_address_of_m_ByteValue_3() { return &___m_ByteValue_3; }
	inline void set_m_ByteValue_3(uint8_t value)
	{
		___m_ByteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_SByteValue_4() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_SByteValue_4)); }
	inline int8_t get_m_SByteValue_4() const { return ___m_SByteValue_4; }
	inline int8_t* get_address_of_m_SByteValue_4() { return &___m_SByteValue_4; }
	inline void set_m_SByteValue_4(int8_t value)
	{
		___m_SByteValue_4 = value;
	}

	inline static int32_t get_offset_of_m_ShortValue_5() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ShortValue_5)); }
	inline int16_t get_m_ShortValue_5() const { return ___m_ShortValue_5; }
	inline int16_t* get_address_of_m_ShortValue_5() { return &___m_ShortValue_5; }
	inline void set_m_ShortValue_5(int16_t value)
	{
		___m_ShortValue_5 = value;
	}

	inline static int32_t get_offset_of_m_UShortValue_6() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_UShortValue_6)); }
	inline uint16_t get_m_UShortValue_6() const { return ___m_UShortValue_6; }
	inline uint16_t* get_address_of_m_UShortValue_6() { return &___m_UShortValue_6; }
	inline void set_m_UShortValue_6(uint16_t value)
	{
		___m_UShortValue_6 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_7() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_IntValue_7)); }
	inline int32_t get_m_IntValue_7() const { return ___m_IntValue_7; }
	inline int32_t* get_address_of_m_IntValue_7() { return &___m_IntValue_7; }
	inline void set_m_IntValue_7(int32_t value)
	{
		___m_IntValue_7 = value;
	}

	inline static int32_t get_offset_of_m_UIntValue_8() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_UIntValue_8)); }
	inline uint32_t get_m_UIntValue_8() const { return ___m_UIntValue_8; }
	inline uint32_t* get_address_of_m_UIntValue_8() { return &___m_UIntValue_8; }
	inline void set_m_UIntValue_8(uint32_t value)
	{
		___m_UIntValue_8 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_9() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_LongValue_9)); }
	inline int64_t get_m_LongValue_9() const { return ___m_LongValue_9; }
	inline int64_t* get_address_of_m_LongValue_9() { return &___m_LongValue_9; }
	inline void set_m_LongValue_9(int64_t value)
	{
		___m_LongValue_9 = value;
	}

	inline static int32_t get_offset_of_m_ULongValue_10() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ULongValue_10)); }
	inline uint64_t get_m_ULongValue_10() const { return ___m_ULongValue_10; }
	inline uint64_t* get_address_of_m_ULongValue_10() { return &___m_ULongValue_10; }
	inline void set_m_ULongValue_10(uint64_t value)
	{
		___m_ULongValue_10 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_11() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_FloatValue_11)); }
	inline float get_m_FloatValue_11() const { return ___m_FloatValue_11; }
	inline float* get_address_of_m_FloatValue_11() { return &___m_FloatValue_11; }
	inline void set_m_FloatValue_11(float value)
	{
		___m_FloatValue_11 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_12() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_DoubleValue_12)); }
	inline double get_m_DoubleValue_12() const { return ___m_DoubleValue_12; }
	inline double* get_address_of_m_DoubleValue_12() { return &___m_DoubleValue_12; }
	inline void set_m_DoubleValue_12(double value)
	{
		___m_DoubleValue_12 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};

// UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F 
{
public:
	// UnityEngine.InputSystem.Utilities.JsonParser/JsonValueType UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::type
	int32_t ___type_0;
	// System.Boolean UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::boolValue
	bool ___boolValue_1;
	// System.Double UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::realValue
	double ___realValue_2;
	// System.Int64 UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::integerValue
	int64_t ___integerValue_3;
	// UnityEngine.InputSystem.Utilities.JsonParser/JsonString UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::stringValue
	JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D  ___stringValue_4;
	// System.Collections.Generic.List`1<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue> UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::arrayValue
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___arrayValue_5;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.InputSystem.Utilities.JsonParser/JsonValue> UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::objectValue
	Dictionary_2_t6559C3595494ACAFACFBD50DF3795F25E9D5FEB1 * ___objectValue_6;
	// System.Object UnityEngine.InputSystem.Utilities.JsonParser/JsonValue::anyValue
	RuntimeObject * ___anyValue_7;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_boolValue_1() { return static_cast<int32_t>(offsetof(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F, ___boolValue_1)); }
	inline bool get_boolValue_1() const { return ___boolValue_1; }
	inline bool* get_address_of_boolValue_1() { return &___boolValue_1; }
	inline void set_boolValue_1(bool value)
	{
		___boolValue_1 = value;
	}

	inline static int32_t get_offset_of_realValue_2() { return static_cast<int32_t>(offsetof(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F, ___realValue_2)); }
	inline double get_realValue_2() const { return ___realValue_2; }
	inline double* get_address_of_realValue_2() { return &___realValue_2; }
	inline void set_realValue_2(double value)
	{
		___realValue_2 = value;
	}

	inline static int32_t get_offset_of_integerValue_3() { return static_cast<int32_t>(offsetof(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F, ___integerValue_3)); }
	inline int64_t get_integerValue_3() const { return ___integerValue_3; }
	inline int64_t* get_address_of_integerValue_3() { return &___integerValue_3; }
	inline void set_integerValue_3(int64_t value)
	{
		___integerValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F, ___stringValue_4)); }
	inline JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D  get_stringValue_4() const { return ___stringValue_4; }
	inline JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D * get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D  value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_arrayValue_5() { return static_cast<int32_t>(offsetof(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F, ___arrayValue_5)); }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * get_arrayValue_5() const { return ___arrayValue_5; }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E ** get_address_of_arrayValue_5() { return &___arrayValue_5; }
	inline void set_arrayValue_5(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * value)
	{
		___arrayValue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrayValue_5), (void*)value);
	}

	inline static int32_t get_offset_of_objectValue_6() { return static_cast<int32_t>(offsetof(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F, ___objectValue_6)); }
	inline Dictionary_2_t6559C3595494ACAFACFBD50DF3795F25E9D5FEB1 * get_objectValue_6() const { return ___objectValue_6; }
	inline Dictionary_2_t6559C3595494ACAFACFBD50DF3795F25E9D5FEB1 ** get_address_of_objectValue_6() { return &___objectValue_6; }
	inline void set_objectValue_6(Dictionary_2_t6559C3595494ACAFACFBD50DF3795F25E9D5FEB1 * value)
	{
		___objectValue_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectValue_6), (void*)value);
	}

	inline static int32_t get_offset_of_anyValue_7() { return static_cast<int32_t>(offsetof(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F, ___anyValue_7)); }
	inline RuntimeObject * get_anyValue_7() const { return ___anyValue_7; }
	inline RuntimeObject ** get_address_of_anyValue_7() { return &___anyValue_7; }
	inline void set_anyValue_7(RuntimeObject * value)
	{
		___anyValue_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anyValue_7), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F_marshaled_pinvoke
{
	int32_t ___type_0;
	int32_t ___boolValue_1;
	double ___realValue_2;
	int64_t ___integerValue_3;
	JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D_marshaled_pinvoke ___stringValue_4;
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___arrayValue_5;
	Dictionary_2_t6559C3595494ACAFACFBD50DF3795F25E9D5FEB1 * ___objectValue_6;
	Il2CppIUnknown* ___anyValue_7;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.JsonParser/JsonValue
struct JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F_marshaled_com
{
	int32_t ___type_0;
	int32_t ___boolValue_1;
	double ___realValue_2;
	int64_t ___integerValue_3;
	JsonString_tE7492E001C8244FE43FD5FE550B2E4471E32364D_marshaled_com ___stringValue_4;
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___arrayValue_5;
	Dictionary_2_t6559C3595494ACAFACFBD50DF3795F25E9D5FEB1 * ___objectValue_6;
	Il2CppIUnknown* ___anyValue_7;
};

// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>
struct Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5, ___list_0)); }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * get_list_0() const { return ___list_0; }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5, ___current_3)); }
	inline JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  get_current_3() const { return ___current_3; }
	inline JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Func`2<UnityEngine.ProBuilder.Edge,System.Boolean>
struct Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.ProBuilder.EdgeLookup,System.Boolean>
struct Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.ProBuilder.EdgeLookup,System.Int32>
struct Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,UnityEngine.ProBuilder.Edge>
struct Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,System.Int32>
struct Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,System.Object>
struct Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,UnityEngine.Vector3>
struct Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Boolean>
struct Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.ProBuilder.Edge>
struct Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Int32>
struct Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>
struct Func_2_tB442E424B8C858730982BF0E6EEA478246930263  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.Vector3>
struct Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Boolean>
struct Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.ProBuilder.Edge>
struct Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Int32>
struct Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.Vector3>
struct Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,UnityEngine.ProBuilder.Edge>
struct Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,UnityEngine.ProBuilder.EdgeLookup>
struct Func_2_t675406C4352EE58B2C120699148BCA7644B247F3  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Int32>
struct Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,UnityEngine.Vector3>
struct Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Boolean>
struct Func_2_t914F2382286698ABCF7602CD671A49AD142738A8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.ProBuilder.Edge>
struct Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Int32>
struct Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>
struct Func_2_t005E8EF5E159894D8474A3B2F511313000B11680  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.Vector3>
struct Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector2,System.Boolean>
struct Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector2,UnityEngine.ProBuilder.Edge>
struct Func_2_t88AB91BBB6E00DC11936250A0515012606223C66  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector2,System.Int32>
struct Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector2,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector2,System.Object>
struct Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector2,UnityEngine.Vector3>
struct Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector3,System.Boolean>
struct Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector4,System.Boolean>
struct Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector4,System.Object>
struct Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Vector4,UnityEngine.Vector2>
struct Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Boolean>
struct Func_2_tF3B895913B44A5233F386097973299392788EA81  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.ProBuilder.Edge>
struct Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Int32>
struct Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.Vector3>
struct Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967  : public MulticastDelegate_t
{
public:

public:
};


// System.Linq.Enumerable/WhereEnumerableIterator`1<UnityEngine.ProBuilder.EdgeLookup>
struct WhereEnumerableIterator_1_tF312478F348A90AF8BFDDD1376F7626D52A5E61F  : public Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereEnumerableIterator`1::predicate
	Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tF312478F348A90AF8BFDDD1376F7626D52A5E61F, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tF312478F348A90AF8BFDDD1376F7626D52A5E61F, ___predicate_4)); }
	inline Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tF312478F348A90AF8BFDDD1376F7626D52A5E61F, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.ProBuilder.EdgeLookup,System.Int32>
struct WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA, ___source_3)); }
	inline List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * get_source_3() const { return ___source_3; }
	inline List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA, ___predicate_4)); }
	inline Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA, ___selector_5)); }
	inline Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA, ___enumerator_6)); }
	inline Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75, ___source_3)); }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * get_source_3() const { return ___source_3; }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75, ___predicate_4)); }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75, ___selector_5)); }
	inline Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75, ___enumerator_6)); }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Int32>
struct WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0, ___source_3)); }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * get_source_3() const { return ___source_3; }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0, ___predicate_4)); }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0, ___selector_5)); }
	inline Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0, ___enumerator_6)); }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632, ___source_3)); }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * get_source_3() const { return ___source_3; }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632, ___predicate_4)); }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632, ___selector_5)); }
	inline Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F * get_selector_5() const { return ___selector_5; }
	inline Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632, ___enumerator_6)); }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>
struct WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724, ___source_3)); }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * get_source_3() const { return ___source_3; }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724, ___predicate_4)); }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724, ___selector_5)); }
	inline Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C * get_selector_5() const { return ___selector_5; }
	inline Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724, ___enumerator_6)); }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.Vector3>
struct WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253, ___source_3)); }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * get_source_3() const { return ___source_3; }
	inline List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253, ___predicate_4)); }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253, ___selector_5)); }
	inline Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253, ___enumerator_6)); }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.EdgeLookup>
struct WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4  : public Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4, ___selector_5)); }
	inline Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NamedValue::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Utilities.NamedValue::<value>k__BackingField
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  ___U3CvalueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94, ___U3CvalueU3Ek__BackingField_2)); }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  get_U3CvalueU3Ek__BackingField_2() const { return ___U3CvalueU3Ek__BackingField_2; }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA * get_address_of_U3CvalueU3Ek__BackingField_2() { return &___U3CvalueU3Ek__BackingField_2; }
	inline void set_U3CvalueU3Ek__BackingField_2(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  value)
	{
		___U3CvalueU3Ek__BackingField_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA_marshaled_pinvoke ___U3CvalueU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA_marshaled_com ___U3CvalueU3Ek__BackingField_2;
};

// System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>
struct Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE, ___list_0)); }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * get_list_0() const { return ___list_0; }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE, ___current_3)); }
	inline NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  get_current_3() const { return ___current_3; }
	inline NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
	}
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Boolean>
struct Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.ProBuilder.Edge>
struct Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Int32>
struct Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.Vector3>
struct Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4  : public MulticastDelegate_t
{
public:

public:
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B, ___source_3)); }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * get_source_3() const { return ___source_3; }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B, ___predicate_4)); }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tF3B895913B44A5233F386097973299392788EA81 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B, ___selector_5)); }
	inline Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B, ___enumerator_6)); }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Int32>
struct WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1, ___source_3)); }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * get_source_3() const { return ___source_3; }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1, ___predicate_4)); }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tF3B895913B44A5233F386097973299392788EA81 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1, ___selector_5)); }
	inline Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1, ___enumerator_6)); }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21, ___source_3)); }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * get_source_3() const { return ___source_3; }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21, ___predicate_4)); }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tF3B895913B44A5233F386097973299392788EA81 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21, ___selector_5)); }
	inline Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21, ___enumerator_6)); }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>
struct WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A, ___source_3)); }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * get_source_3() const { return ___source_3; }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A, ___predicate_4)); }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tF3B895913B44A5233F386097973299392788EA81 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A, ___selector_5)); }
	inline Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC * get_selector_5() const { return ___selector_5; }
	inline Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A, ___enumerator_6)); }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC, ___source_3)); }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * get_source_3() const { return ___source_3; }
	inline List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC, ___predicate_4)); }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tF3B895913B44A5233F386097973299392788EA81 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tF3B895913B44A5233F386097973299392788EA81 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC, ___selector_5)); }
	inline Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC, ___enumerator_6)); }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___enumerator_6))->___current_3))->___stringValue_4))->___text_0))->___m_String_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___arrayValue_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___objectValue_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___anyValue_7), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.ProBuilder.Edge>
struct WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11  : public Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11, ___source_3)); }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * get_source_3() const { return ___source_3; }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11, ___predicate_4)); }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11, ___selector_5)); }
	inline Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11, ___enumerator_6)); }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Int32>
struct WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523, ___source_3)); }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * get_source_3() const { return ___source_3; }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523, ___predicate_4)); }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523, ___selector_5)); }
	inline Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523, ___enumerator_6)); }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>
struct WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A  : public Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A, ___source_3)); }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * get_source_3() const { return ___source_3; }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A, ___predicate_4)); }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A, ___selector_5)); }
	inline Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A, ___enumerator_6)); }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>
struct WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D, ___source_3)); }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * get_source_3() const { return ___source_3; }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D, ___predicate_4)); }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D, ___selector_5)); }
	inline Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D, ___enumerator_6)); }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.Vector3>
struct WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9  : public Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::source
	List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable/WhereSelectListIterator`2::predicate
	Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/WhereSelectListIterator`2::selector
	Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 * ___selector_5;
	// System.Collections.Generic.List`1/Enumerator<TSource> System.Linq.Enumerable/WhereSelectListIterator`2::enumerator
	Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9, ___source_3)); }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * get_source_3() const { return ___source_3; }
	inline List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9, ___predicate_4)); }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9, ___selector_5)); }
	inline Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9, ___enumerator_6)); }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___U3CnameU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.ProBuilder.EdgeLookup>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  Enumerator_get_Current_m5D58E98BBF2B7DFDA87005EACBA952CF220E8EAC_gshared_inline (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.ProBuilder.EdgeLookup>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m8B42A02F3E3CF0DADFEEB3D05B821D0E5FB178BD_gshared (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_gshared (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_gshared_inline (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m696588146F8F5AF4AE2C00A5B5BD9A05A1398131_gshared (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_gshared_inline (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m74691FC3D69F784B093B175B170ED95C6FACE14B_gshared (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_gshared_inline (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mE66B652AA14C8DF3462B3A6EFF64ED50A0451E91_gshared (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_gshared_inline (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m4B93FB44F564B9B972BD3FFC30EBA3922B766501_gshared (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_gshared_inline (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m7A577B0782F0D174CEA921C7B67075BD60034A6C_gshared (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  Enumerator_get_Current_m32439407464C10970CC963587D0D2E0DD861ED9B_gshared_inline (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mF378337D9CA43F03755456627ECC0436E94A8B9C_gshared (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_gshared_inline (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m262885D7B2625EA9D3DB985226C942342C65B3F7_gshared (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * __this, const RuntimeMethod* method);

// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.ProBuilder.EdgeLookup>::get_Current()
inline EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  Enumerator_get_Current_m5D58E98BBF2B7DFDA87005EACBA952CF220E8EAC_inline (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 * __this, const RuntimeMethod* method)
{
	return ((  EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  (*) (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 *, const RuntimeMethod*))Enumerator_get_Current_m5D58E98BBF2B7DFDA87005EACBA952CF220E8EAC_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.ProBuilder.EdgeLookup>::MoveNext()
inline bool Enumerator_MoveNext_m8B42A02F3E3CF0DADFEEB3D05B821D0E5FB178BD (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 *, const RuntimeMethod*))Enumerator_MoveNext_m8B42A02F3E3CF0DADFEEB3D05B821D0E5FB178BD_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
inline int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *, const RuntimeMethod*))Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
inline bool Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6 (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *, const RuntimeMethod*))Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::get_Current()
inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_inline (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * __this, const RuntimeMethod* method)
{
	return ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *, const RuntimeMethod*))Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
inline bool Enumerator_MoveNext_m696588146F8F5AF4AE2C00A5B5BD9A05A1398131 (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *, const RuntimeMethod*))Enumerator_MoveNext_m696588146F8F5AF4AE2C00A5B5BD9A05A1398131_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Current()
inline NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_inline (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * __this, const RuntimeMethod* method)
{
	return ((  NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  (*) (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *, const RuntimeMethod*))Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NameAndParameters>::MoveNext()
inline bool Enumerator_MoveNext_m74691FC3D69F784B093B175B170ED95C6FACE14B (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *, const RuntimeMethod*))Enumerator_MoveNext_m74691FC3D69F784B093B175B170ED95C6FACE14B_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::get_Current()
inline NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_inline (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * __this, const RuntimeMethod* method)
{
	return ((  NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  (*) (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *, const RuntimeMethod*))Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.NamedValue>::MoveNext()
inline bool Enumerator_MoveNext_mE66B652AA14C8DF3462B3A6EFF64ED50A0451E91 (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *, const RuntimeMethod*))Enumerator_MoveNext_mE66B652AA14C8DF3462B3A6EFF64ED50A0451E91_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
inline RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
inline bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0 (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::get_Current()
inline Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_inline (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * __this, const RuntimeMethod* method)
{
	return ((  Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  (*) (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *, const RuntimeMethod*))Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.Substring>::MoveNext()
inline bool Enumerator_MoveNext_m4B93FB44F564B9B972BD3FFC30EBA3922B766501 (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *, const RuntimeMethod*))Enumerator_MoveNext_m4B93FB44F564B9B972BD3FFC30EBA3922B766501_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_inline (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * __this, const RuntimeMethod* method)
{
	return ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *, const RuntimeMethod*))Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
inline bool Enumerator_MoveNext_m7A577B0782F0D174CEA921C7B67075BD60034A6C (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *, const RuntimeMethod*))Enumerator_MoveNext_m7A577B0782F0D174CEA921C7B67075BD60034A6C_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  Enumerator_get_Current_m32439407464C10970CC963587D0D2E0DD861ED9B_inline (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * __this, const RuntimeMethod* method)
{
	return ((  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  (*) (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *, const RuntimeMethod*))Enumerator_get_Current_m32439407464C10970CC963587D0D2E0DD861ED9B_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
inline bool Enumerator_MoveNext_mF378337D9CA43F03755456627ECC0436E94A8B9C (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *, const RuntimeMethod*))Enumerator_MoveNext_mF378337D9CA43F03755456627ECC0436E94A8B9C_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::get_Current()
inline JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_inline (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * __this, const RuntimeMethod* method)
{
	return ((  JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  (*) (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *, const RuntimeMethod*))Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue>::MoveNext()
inline bool Enumerator_MoveNext_m262885D7B2625EA9D3DB985226C942342C65B3F7 (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *, const RuntimeMethod*))Enumerator_MoveNext_m262885D7B2625EA9D3DB985226C942342C65B3F7_gshared)(__this, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.ProBuilder.EdgeLookup,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mF3B2FB58331F005CCC923AC5A8A40B89D5615C9E_gshared (WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA * __this, List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * ___source0, Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * ___predicate1, Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.ProBuilder.EdgeLookup,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_m9B2D3BCDAEEBC50C3A0E6E02074CFBC535D776ED_gshared (WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA * __this, const RuntimeMethod* method)
{
	{
		List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * L_0 = (List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 *)__this->get_source_3();
		Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * L_1 = (Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *)__this->get_predicate_4();
		Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 * L_2 = (Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 *)__this->get_selector_5();
		WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA * L_3 = (WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA *, List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 *, Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *, Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 *)L_0, (Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *)L_1, (Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.ProBuilder.EdgeLookup,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mDCB74A4529322154A43CDE8035FFAAB08F063537_gshared (WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 * L_3 = (List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 *)__this->get_source_3();
		NullCheck((List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 *)L_3);
		Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455  L_4;
		L_4 = ((  Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455  (*) (List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t06FEDAB8C1985D68F1C55AFBB76E7875647617D2 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 * L_5 = (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 *)__this->get_address_of_enumerator_6();
		EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  L_6;
		L_6 = Enumerator_get_Current_m5D58E98BBF2B7DFDA87005EACBA952CF220E8EAC_inline((Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 *)(Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A )L_6;
		Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * L_7 = (Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * L_8 = (Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *)__this->get_predicate_4();
		EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  L_9 = V_1;
		NullCheck((Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *, EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *)L_8, (EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 * L_11 = (Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 *)__this->get_selector_5();
		EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  L_12 = V_1;
		NullCheck((Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 *, EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tABCFBA591DEBC7C152DCC7FD497B5FC45A49B013 *)L_11, (EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 * L_14 = (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m8B42A02F3E3CF0DADFEEB3D05B821D0E5FB178BD((Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 *)(Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.ProBuilder.EdgeLookup,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m1309D5E85B4782406F94267FF956A27A17FE118C_gshared (WhereSelectListIterator_2_tBACC89AC876933F393E4A9958B1AA4E1C9E3D8EA * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.ProBuilder.Edge>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m5A516160FD466D4835C375F698060CE29FBB9D0E_gshared (WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source0, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate1, Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		((  void (*) (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.ProBuilder.Edge>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 * WhereSelectListIterator_2_Clone_m7FF79E4B6B1DF57EF24595509727C693AD714602_gshared (WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E * L_2 = (Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E *)__this->get_selector_5();
		WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C * L_3 = (WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C *, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_0, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_1, (Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.ProBuilder.Edge>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m620F18D1918E719732ACB0B0BBDEF3436870C581_gshared (WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		NullCheck((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3);
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_4;
		L_4 = ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_5 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		int32_t L_6;
		L_6 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_7 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_8 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E * L_11 = (Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E *)L_11);
		Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  L_13;
		L_13 = ((  Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  (*) (Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t49716CF1E61F3F72723EC278259DAC19AF57011E *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_14 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>::Dispose() */, (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.ProBuilder.Edge>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m5E3EA9CA83582DCC0C85F8134603943AE44FC9DD_gshared (WhereSelectListIterator_2_tB661931796C1C38EFBFA98A7054FB91A8200B94C * __this, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 * L_1 = (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *, RuntimeObject*, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m895E21AE9AB1E3F19B3147EDC913BB567B1A65C7_gshared (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source0, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate1, Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mEE6375B2C79172E13732CA49AAF389493C1C7100_gshared (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_2 = (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)__this->get_selector_5();
		WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * L_3 = (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 *, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_0, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_1, (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m637B802A50BA94CD511636CAF5D912C6B96B18A1_gshared (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		NullCheck((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3);
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_4;
		L_4 = ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_5 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		int32_t L_6;
		L_6 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_7 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_8 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_11 = (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_14 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m7F75FF628D2E99D2BA127B84FDD08DD88048ADB0_gshared (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m7A10B61C7744C9B4C0F1A51FB7DAD7BE1D53B873_gshared (WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4 * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source0, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate1, Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		((  void (*) (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB * WhereSelectListIterator_2_Clone_m7412982A546030A9516219EE1BB1EA4F2DCF1E20_gshared (WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4 * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 * L_2 = (Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 *)__this->get_selector_5();
		WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4 * L_3 = (WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4 *, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_0, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_1, (Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m16DE859F577E95688C5C2C89A55AD74E7F9E8348_gshared (WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		NullCheck((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3);
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_4;
		L_4 = ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_5 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		int32_t L_6;
		L_6 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_7 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_8 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 * L_11 = (Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 *)L_11);
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_13;
		L_13 = ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t81AB47DC722EB251FDCAD800B1FBAD8A41B4D941 *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_14 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m895EF4CE8826C41E80D242C0327649C8D65AA2EE_gshared (WhereSelectListIterator_2_t62D4A38CE0296C96F76FF4EAACA4276843B09AC4 * __this, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 * L_1 = (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *, RuntimeObject*, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mA0D61B688D5EE4E84300EA96C87ED9F3E10D5EA9_gshared (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source0, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate1, Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mE40F022902D030D86E465678E5DD79B3058FE2CB_gshared (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * L_2 = (Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)__this->get_selector_5();
		WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * L_3 = (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 *, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_0, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_1, (Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mDEB78D7AB98D0FDC13661615FDBC0C01A621E84F_gshared (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		NullCheck((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3);
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_4;
		L_4 = ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_5 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		int32_t L_6;
		L_6 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_7 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_8 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * L_11 = (Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_14 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m9633A851E09C546940C2D5C7EF8CB7C501784EB3_gshared (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m25E3803D7DE0995957AB317131C2C36A7FE1B388_gshared (WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5 * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source0, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate1, Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		((  void (*) (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.Vector3>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF * WhereSelectListIterator_2_Clone_m6A17177E9DDB093932115306CED6132F54F54914_gshared (WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5 * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 * L_2 = (Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 *)__this->get_selector_5();
		WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5 * L_3 = (WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5 *, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_0, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_1, (Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.Vector3>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mB02EFC9D649FC65C3FA6F2BAAC202CDCC1E1DDB1_gshared (WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		NullCheck((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3);
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_4;
		L_4 = ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_5 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		int32_t L_6;
		L_6 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_7 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_8 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 * L_11 = (Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 *)L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tD796940AF77AEAB4FF6D2FD430475019E7527FE2 *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_14 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>::Dispose() */, (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,UnityEngine.Vector3>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m3E597F72FE79EE1378DB04DC8258C06460B87999_gshared (WhereSelectListIterator_2_t465AA0CA0C14F710B65F6ECC60153C407F9AE1F5 * __this, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 * L_1 = (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *, RuntimeObject*, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.ProBuilder.Edge>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mEBF3D6DAC1D2E05AD72BDF247B958625378A8D62_gshared (WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19 * __this, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source0, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate1, Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		((  void (*) (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.ProBuilder.Edge>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 * WhereSelectListIterator_2_Clone_m36CB978943B01E6BC4B07A8AD8445399B2356685_gshared (WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19 * __this, const RuntimeMethod* method)
{
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE * L_2 = (Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE *)__this->get_selector_5();
		WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19 * L_3 = (WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19 *, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_0, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_1, (Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.ProBuilder.Edge>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m2A0BBACD7398308CD04F9C0D6215E514308AC938_gshared (WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_3 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		NullCheck((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3);
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  L_4;
		L_4 = ((  Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  (*) (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_5 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_6;
		L_6 = Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_inline((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_6;
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_7 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_8 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_9 = V_1;
		NullCheck((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE * L_11 = (Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE *)__this->get_selector_5();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_12 = V_1;
		NullCheck((Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE *)L_11);
		Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  L_13;
		L_13 = ((  Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  (*) (Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6F843C378F91BF194D05E13C3D4845B19B5215DE *)L_11, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_14 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m696588146F8F5AF4AE2C00A5B5BD9A05A1398131((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>::Dispose() */, (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.ProBuilder.Edge>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m755582E05970326DFF11BD9A6ACC42501E8E03F4_gshared (WhereSelectListIterator_2_t915E025CF49D8B15F23CCBA667EF07F2623B1B19 * __this, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 * L_1 = (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *, RuntimeObject*, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mC38E41AA7B240EE0898E35924BE4736ADFE47139_gshared (WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C * __this, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source0, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate1, Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mDA3E79F9EA9E284AE589BB8072D124804D1D3D7B_gshared (WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C * __this, const RuntimeMethod* method)
{
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 * L_2 = (Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 *)__this->get_selector_5();
		WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C * L_3 = (WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C *, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_0, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_1, (Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m32CD9BB41534EC4BDB4EB62589D5D64D55FBF94B_gshared (WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_3 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		NullCheck((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3);
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  L_4;
		L_4 = ((  Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  (*) (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_5 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_6;
		L_6 = Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_inline((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_6;
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_7 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_8 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_9 = V_1;
		NullCheck((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 * L_11 = (Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 *)__this->get_selector_5();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_12 = V_1;
		NullCheck((Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t79D7E733C35403FBD2A52C18EB6173492D0F9176 *)L_11, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_14 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m696588146F8F5AF4AE2C00A5B5BD9A05A1398131((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mA4D530A16156253EDD9C133031EDCCE0387A7ACA_gshared (WhereSelectListIterator_2_t78F2A06F62A086C8D698943B8DFB38F72C60591C * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m475FF7BDFC83022CF87E5FE140DE480615875DFD_gshared (WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144 * __this, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source0, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate1, Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		((  void (*) (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB * WhereSelectListIterator_2_Clone_m01C0612F51350D4B910C2D361A647E85080609D8_gshared (WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144 * __this, const RuntimeMethod* method)
{
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC * L_2 = (Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC *)__this->get_selector_5();
		WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144 * L_3 = (WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144 *, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_0, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_1, (Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m8E55C5FCC540AB385EEB5E5E2A7E778A4D028613_gshared (WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_3 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		NullCheck((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3);
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  L_4;
		L_4 = ((  Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  (*) (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_5 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_6;
		L_6 = Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_inline((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_6;
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_7 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_8 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_9 = V_1;
		NullCheck((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC * L_11 = (Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC *)__this->get_selector_5();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_12 = V_1;
		NullCheck((Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC *)L_11);
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_13;
		L_13 = ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t8D91F9DA2D1402220CA7B600B7E53A436069DFDC *)L_11, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_14 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m696588146F8F5AF4AE2C00A5B5BD9A05A1398131((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mE709BA79D41F3158657A668DD32E9F054FAB8463_gshared (WhereSelectListIterator_2_t5AD2BDF3AC9802910606C7224CCD3F4C36777144 * __this, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 * L_1 = (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *, RuntimeObject*, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mF423085032A5AA619BB4A03B0035DD1DD0CEFC98_gshared (WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960 * __this, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source0, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate1, Func_2_tB442E424B8C858730982BF0E6EEA478246930263 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB442E424B8C858730982BF0E6EEA478246930263 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m6BAD8B2915F8C4D6EAF2E1D4B16A1379CADBC3A5_gshared (WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960 * __this, const RuntimeMethod* method)
{
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		Func_2_tB442E424B8C858730982BF0E6EEA478246930263 * L_2 = (Func_2_tB442E424B8C858730982BF0E6EEA478246930263 *)__this->get_selector_5();
		WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960 * L_3 = (WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960 *, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, Func_2_tB442E424B8C858730982BF0E6EEA478246930263 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_0, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_1, (Func_2_tB442E424B8C858730982BF0E6EEA478246930263 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m4D90AE6FD6D02558C1BDFC4018609A2A908DCD0D_gshared (WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_3 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		NullCheck((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3);
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  L_4;
		L_4 = ((  Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  (*) (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_5 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_6;
		L_6 = Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_inline((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_6;
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_7 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_8 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_9 = V_1;
		NullCheck((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB442E424B8C858730982BF0E6EEA478246930263 * L_11 = (Func_2_tB442E424B8C858730982BF0E6EEA478246930263 *)__this->get_selector_5();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_12 = V_1;
		NullCheck((Func_2_tB442E424B8C858730982BF0E6EEA478246930263 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tB442E424B8C858730982BF0E6EEA478246930263 *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tB442E424B8C858730982BF0E6EEA478246930263 *)L_11, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_14 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m696588146F8F5AF4AE2C00A5B5BD9A05A1398131((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mD06746CB3DFDB71E5EBD31AD0A5043EDF56550AA_gshared (WhereSelectListIterator_2_t122290D5D17C1F29C9A126AB8781C96567249960 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m01E6F9BC9886DAE84D582307DF465F8376B1B875_gshared (WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D * __this, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * ___source0, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate1, Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		((  void (*) (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.Vector3>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF * WhereSelectListIterator_2_Clone_mA1B3FCCCF6D246691CD859385F636D2BF0066B7C_gshared (WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D * __this, const RuntimeMethod* method)
{
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_0 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_1 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 * L_2 = (Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 *)__this->get_selector_5();
		WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D * L_3 = (WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D *, List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_0, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_1, (Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.Vector3>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m134D8919AB105029553AB835B0F131FF67DB84A1_gshared (WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 * L_3 = (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)__this->get_source_3();
		NullCheck((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3);
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  L_4;
		L_4 = ((  Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3  (*) (List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t4F7BD143D06100371DF9528294BF98BC7BF87371 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_5 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_6;
		L_6 = Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_inline((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_6;
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_7 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_8 = (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)__this->get_predicate_4();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_9 = V_1;
		NullCheck((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_8, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 * L_11 = (Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 *)__this->get_selector_5();
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_12 = V_1;
		NullCheck((Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 *)L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 *, InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tB9C578D5A5D6F952788F74FD5D083BDE30EB2014 *)L_11, (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * L_14 = (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m696588146F8F5AF4AE2C00A5B5BD9A05A1398131((Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)(Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>::Dispose() */, (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.Vector3>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mB507E8570A44BF466524FAEDB58618C523AF0CA5_gshared (WhereSelectListIterator_2_t1A72C896AB93C70B031DC9DFD5FC6C5FD24F0B1D * __this, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 * L_1 = (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *, RuntimeObject*, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.ProBuilder.Edge>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCFA640039C374234EB294BB3F3FDEA3731892163_gshared (WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75 * __this, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source0, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate1, Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		((  void (*) (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.ProBuilder.Edge>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 * WhereSelectListIterator_2_Clone_m0D985563AACEAC1664CF4079C420E3D829BD1F11_gshared (WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 * L_2 = (Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 *)__this->get_selector_5();
		WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75 * L_3 = (WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75 *, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_0, (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_1, (Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.ProBuilder.Edge>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m5523310342AF3F16FCA76DB78ED96F0C9A6E0411_gshared (WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_3 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		NullCheck((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3);
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  L_4;
		L_4 = ((  Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  (*) (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_5 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_6;
		L_6 = Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_inline((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_6;
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_7 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_8 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_9 = V_1;
		NullCheck((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 * L_11 = (Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 *)__this->get_selector_5();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_12 = V_1;
		NullCheck((Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 *)L_11);
		Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  L_13;
		L_13 = ((  Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  (*) (Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE10169C6CD19D2B62117A499BA0D4501F979E9B1 *)L_11, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_14 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m74691FC3D69F784B093B175B170ED95C6FACE14B((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>::Dispose() */, (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.ProBuilder.Edge>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m625E7584DD1835A5B56C9CED3AABA6B6DC277E40_gshared (WhereSelectListIterator_2_t37BFA32110FB3A7C2EC725015AAC08D17632AA75 * __this, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 * L_1 = (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *, RuntimeObject*, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m21D7928AEA9BF72276436E0C49F45E4E6353B493_gshared (WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0 * __this, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source0, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate1, Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mAB91B0457AF5F51E7D2B73CC68C3E75917F0EBD1_gshared (WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 * L_2 = (Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 *)__this->get_selector_5();
		WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0 * L_3 = (WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0 *, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_0, (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_1, (Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m399E24416090A69477D228DB187E97567FB49A11_gshared (WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_3 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		NullCheck((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3);
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  L_4;
		L_4 = ((  Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  (*) (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_5 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_6;
		L_6 = Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_inline((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_6;
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_7 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_8 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_9 = V_1;
		NullCheck((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 * L_11 = (Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 *)__this->get_selector_5();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_12 = V_1;
		NullCheck((Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tCB6E9B81A215B64E99CC33333549A7DC44A3B3A2 *)L_11, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_14 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m74691FC3D69F784B093B175B170ED95C6FACE14B((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m22EB45988227AD5D3B2E392BC1F0025CDDEAA679_gshared (WhereSelectListIterator_2_t762537613C574067DA91D03D3219B043D41FC0B0 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mE87EACF1F9B6F263F10D34198B0E38373CDCB975_gshared (WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632 * __this, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source0, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate1, Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		((  void (*) (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB * WhereSelectListIterator_2_Clone_m0A1EE019FA61AB4FB05E81136EB53D360105FC25_gshared (WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F * L_2 = (Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F *)__this->get_selector_5();
		WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632 * L_3 = (WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632 *, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_0, (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_1, (Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mAB09BE67B621E692B295D32834E0284FF4472568_gshared (WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_3 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		NullCheck((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3);
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  L_4;
		L_4 = ((  Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  (*) (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_5 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_6;
		L_6 = Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_inline((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_6;
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_7 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_8 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_9 = V_1;
		NullCheck((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F * L_11 = (Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F *)__this->get_selector_5();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_12 = V_1;
		NullCheck((Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F *)L_11);
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_13;
		L_13 = ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0D6A5638A06EA13C4D7DF4EFB5A599050A11F35F *)L_11, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_14 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m74691FC3D69F784B093B175B170ED95C6FACE14B((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m0C64AEEBC0BD9EC7860C09F00CF98C200B05C5E9_gshared (WhereSelectListIterator_2_tAF6E4394F8DF5E9970FD5D807B0F80C187899632 * __this, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 * L_1 = (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *, RuntimeObject*, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m9CCB2EEB4BDD7B63FF983F88F271CA350DDB77D4_gshared (WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724 * __this, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source0, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate1, Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m30FFF5C7EAC0C83F6E089607199B73E83369AAD2_gshared (WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C * L_2 = (Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C *)__this->get_selector_5();
		WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724 * L_3 = (WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724 *, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_0, (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_1, (Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m5F1A267D617A3074F14B9C2F1D76F760DE15062C_gshared (WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_3 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		NullCheck((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3);
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  L_4;
		L_4 = ((  Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  (*) (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_5 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_6;
		L_6 = Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_inline((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_6;
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_7 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_8 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_9 = V_1;
		NullCheck((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C * L_11 = (Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C *)__this->get_selector_5();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_12 = V_1;
		NullCheck((Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t35C422FD22A61616C87ECA9ABD95BC896CA22B5C *)L_11, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_14 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m74691FC3D69F784B093B175B170ED95C6FACE14B((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m1E3F559067BBC5652F484C114CCC072C3F47CF9C_gshared (WhereSelectListIterator_2_t5DC19157282B9D0F156003306BF5E0881582A724 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m877D36FF7740DD5D7EB9A889E8975288A80D34F6_gshared (WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253 * __this, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * ___source0, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * ___predicate1, Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		((  void (*) (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.Vector3>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF * WhereSelectListIterator_2_Clone_m14C84FC80A697B28726D7DF2E7D5BEC5E555B2A3_gshared (WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253 * __this, const RuntimeMethod* method)
{
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_0 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_1 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B * L_2 = (Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B *)__this->get_selector_5();
		WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253 * L_3 = (WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253 *, List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_0, (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_1, (Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.Vector3>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m4D04710E88C779839339A8B6D06CBA0F2E8BC968_gshared (WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 * L_3 = (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)__this->get_source_3();
		NullCheck((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3);
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  L_4;
		L_4 = ((  Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4  (*) (List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t0254BA1C5820319DE57FFC7479FC56B84460FB50 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_5 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_6;
		L_6 = Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_inline((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_6;
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_7 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 * L_8 = (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)__this->get_predicate_4();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_9 = V_1;
		NullCheck((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t6310D2EAD09946C537C07ED4C2C9F889E4B77364 *)L_8, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B * L_11 = (Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B *)__this->get_selector_5();
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_12 = V_1;
		NullCheck((Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B *)L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B *, NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99B9BB40B6E8E56331E471978F270E2E78AD7E4B *)L_11, (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * L_14 = (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m74691FC3D69F784B093B175B170ED95C6FACE14B((Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)(Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>::Dispose() */, (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.Vector3>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mC1EE7C0A594C6B0C489A3A12F446AF13F8869748_gshared (WhereSelectListIterator_2_tBF058A703F4B096FEDE24BF0C4D49CB229B91253 * __this, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 * L_1 = (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *, RuntimeObject*, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.ProBuilder.Edge>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mDECA2C7B55480DEABB876C7F63ADF595FCA35555_gshared (WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11 * __this, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source0, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate1, Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		((  void (*) (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.ProBuilder.Edge>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 * WhereSelectListIterator_2_Clone_m9371C7000A5C59113868BF25E25D4377D937AC88_gshared (WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11 * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 * L_2 = (Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 *)__this->get_selector_5();
		WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11 * L_3 = (WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11 *, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_0, (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_1, (Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.ProBuilder.Edge>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mCC48E0AA80583FE1FB6BB57A1B78917A7899344C_gshared (WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_3 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		NullCheck((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3);
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  L_4;
		L_4 = ((  Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  (*) (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_5 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_6;
		L_6 = Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_inline((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_6;
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_7 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_8 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_9 = V_1;
		NullCheck((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 * L_11 = (Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 *)__this->get_selector_5();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_12 = V_1;
		NullCheck((Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 *)L_11);
		Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  L_13;
		L_13 = ((  Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  (*) (Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t196F6AEFB35B76859C8F70CDD4C196832765F234 *)L_11, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_14 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mE66B652AA14C8DF3462B3A6EFF64ED50A0451E91((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>::Dispose() */, (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.ProBuilder.Edge>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mD1D67B3FC735F46EE4A273261E360F843DE53568_gshared (WhereSelectListIterator_2_t6C18CB9347BC5CA428CF72951945D3B92CCDAE11 * __this, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 * L_1 = (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *, RuntimeObject*, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m24EE57C34A4DBF6DE6AC2A3522D773ECA32BB2CB_gshared (WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523 * __this, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source0, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate1, Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_m209279B4327753EEF88D94DA33A9B0B4AEF4A860_gshared (WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523 * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D * L_2 = (Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D *)__this->get_selector_5();
		WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523 * L_3 = (WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523 *, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_0, (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_1, (Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m29834B5E92F004BC3E7DD1FC4A73E89F71522279_gshared (WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_3 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		NullCheck((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3);
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  L_4;
		L_4 = ((  Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  (*) (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_5 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_6;
		L_6 = Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_inline((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_6;
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_7 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_8 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_9 = V_1;
		NullCheck((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D * L_11 = (Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D *)__this->get_selector_5();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_12 = V_1;
		NullCheck((Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5A82E307492425DCFE91264F41E721D6BA68B13D *)L_11, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_14 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mE66B652AA14C8DF3462B3A6EFF64ED50A0451E91((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m261D55CB29455E51A77D00D9A36CEE44454887C1_gshared (WhereSelectListIterator_2_t59C3C78595879DACDD7DD902ED11C0B928EC7523 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mF2D61204BAAFEA25013F85024DD7873C02CDC657_gshared (WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A * __this, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source0, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate1, Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		((  void (*) (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB * WhereSelectListIterator_2_Clone_mAF6655F8C7183FC26E7D3EE00D91A66B1714EA9A_gshared (WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 * L_2 = (Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 *)__this->get_selector_5();
		WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A * L_3 = (WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A *, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_0, (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_1, (Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m6C6C2F16130D04310221C5A15EFB972BCB21911B_gshared (WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_3 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		NullCheck((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3);
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  L_4;
		L_4 = ((  Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  (*) (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_5 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_6;
		L_6 = Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_inline((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_6;
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_7 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_8 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_9 = V_1;
		NullCheck((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 * L_11 = (Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 *)__this->get_selector_5();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_12 = V_1;
		NullCheck((Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 *)L_11);
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_13;
		L_13 = ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t17571A7D6C83063E6CD1D7975573F50F55E76106 *)L_11, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_14 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mE66B652AA14C8DF3462B3A6EFF64ED50A0451E91((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mDA827CE06E19916331E208314311CA3D262441B5_gshared (WhereSelectListIterator_2_tF0B6141A57030402DF504376BB0958008614223A * __this, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 * L_1 = (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *, RuntimeObject*, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m641207CFF0D496A3314F655C499CEB455FF4A4EA_gshared (WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D * __this, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source0, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate1, Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mC148AD10FAC7AD3E63596E83C341C871AEAEB6B7_gshared (WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 * L_2 = (Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 *)__this->get_selector_5();
		WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D * L_3 = (WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D *, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_0, (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_1, (Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m2E19E1B3D638FB10A119E716B19F8343B3C5140C_gshared (WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_3 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		NullCheck((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3);
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  L_4;
		L_4 = ((  Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  (*) (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_5 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_6;
		L_6 = Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_inline((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_6;
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_7 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_8 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_9 = V_1;
		NullCheck((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 * L_11 = (Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 *)__this->get_selector_5();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_12 = V_1;
		NullCheck((Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t183C0510F1D3A2E447A65AAF7810C3C7786D6829 *)L_11, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_14 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mE66B652AA14C8DF3462B3A6EFF64ED50A0451E91((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mEA0ADB0CB57F190AA4E187529B8B7471F2AC756F_gshared (WhereSelectListIterator_2_t236873BD546F283A5029C524BD3476F056191C0D * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m2155BB64D3EF92C11B26C9D580B1F95D1345ED40_gshared (WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9 * __this, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * ___source0, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * ___predicate1, Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		((  void (*) (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.Vector3>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF * WhereSelectListIterator_2_Clone_mC86932BA11E11AE8F742B57624772552397639E8_gshared (WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9 * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_0 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_1 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 * L_2 = (Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 *)__this->get_selector_5();
		WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9 * L_3 = (WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9 *, List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_0, (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_1, (Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.Vector3>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mC0278D3EE7C0F9945582B4E1D84F53FF3723C224_gshared (WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A * L_3 = (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)__this->get_source_3();
		NullCheck((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3);
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  L_4;
		L_4 = ((  Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE  (*) (List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tE8A25DA3E6F82A334F4C87EE6DE66AB53D16042A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_5 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_6;
		L_6 = Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_inline((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_6;
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_7 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB * L_8 = (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)__this->get_predicate_4();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_9 = V_1;
		NullCheck((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t35B4C5A2ED4A7D53F2F064944FED94D5E19F9EBB *)L_8, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 * L_11 = (Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 *)__this->get_selector_5();
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_12 = V_1;
		NullCheck((Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 *)L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 *, NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t06ACD4FF053B8307DA2F595D034A918A7395CFB4 *)L_11, (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * L_14 = (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mE66B652AA14C8DF3462B3A6EFF64ED50A0451E91((Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)(Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>::Dispose() */, (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.Vector3>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m434E41285E0F3AFFC86C5A004C32AEEF362549AE_gshared (WhereSelectListIterator_2_t00E26CEA72F1E119E5993BB5154097DEBEA81DE9 * __this, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 * L_1 = (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *, RuntimeObject*, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.Edge>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mFF268E743BE233CA8761C66651BE04B05BEDA3CE_gshared (WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		((  void (*) (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.Edge>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 * WhereSelectListIterator_2_Clone_m1BE6A6E848729B66F8661876488B05D18EB41F60_gshared (WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF * L_2 = (Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF *)__this->get_selector_5();
		WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8 * L_3 = (WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.Edge>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m3295C711E473F98F76A1DAD02F92891067ED32DF_gshared (WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF * L_11 = (Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF *)L_11);
		Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  L_13;
		L_13 = ((  Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  (*) (Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tED75C53F162C51C53AF2FC071678F516F6EBC0CF *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>::Dispose() */, (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.Edge>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m1AAC45BC1269168D8CA5611901F09D73DB4DFB7B_gshared (WhereSelectListIterator_2_tC63174BF31CF6DFA55073EF96E570FF9AB0DFBD8 * __this, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 * L_1 = (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *, RuntimeObject*, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.EdgeLookup>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m6745900F048D82BFD4B863E2A57EBDFE4AC94F8E_gshared (WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *)__this);
		((  void (*) (Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.EdgeLookup>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F * WhereSelectListIterator_2_Clone_m64A154D4BD7977DFBDAB08FE9F21B90251DBD48F_gshared (WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 * L_2 = (Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 *)__this->get_selector_5();
		WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4 * L_3 = (WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.EdgeLookup>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mB9DAC96765BC95DB10C1BE61B134561640EAC1B2_gshared (WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 * L_11 = (Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 *)L_11);
		EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  L_13;
		L_13 = ((  EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  (*) (Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t675406C4352EE58B2C120699148BCA7644B247F3 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.EdgeLookup>::Dispose() */, (Iterator_1_tACFEB211A8111274E641486E67A27E8D236BDE2F *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.ProBuilder.EdgeLookup>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m800FC35AA9CF180C1B32F3882609A3911D025084_gshared (WhereSelectListIterator_2_tD33D10C36B31C1B131128EE0DC2484C8BAACA5C4 * __this, Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tF312478F348A90AF8BFDDD1376F7626D52A5E61F * L_1 = (WhereEnumerableIterator_1_tF312478F348A90AF8BFDDD1376F7626D52A5E61F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tF312478F348A90AF8BFDDD1376F7626D52A5E61F *, RuntimeObject*, Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t764F55A0D6E4CAAF2AF7110E8E5B46843E7BDB10 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mA18E5B755DF06EAE14007C295ACE48C7E0615BAE_gshared (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mFEFF616AB589FF68AA4BCFE920814C287AC50268_gshared (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * L_2 = (Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)__this->get_selector_5();
		WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * L_3 = (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m758147C75B5A6CB8C86D4FEA039965940D0BDD6C_gshared (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * L_11 = (Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mB03479084DC9989DEAC9938DC78329DC769EFA14_gshared (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m550D2BF529E7468E5B9E4F55AE58F38A67013BB0_gshared (WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		((  void (*) (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB * WhereSelectListIterator_2_Clone_m14BAB2A291928374C9F85E5C0BD1A87A6953D8E5_gshared (WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 * L_2 = (Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 *)__this->get_selector_5();
		WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD * L_3 = (WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mF0F24A7650A1F4BAD46E9CED87A5D3F0C4C255A5_gshared (WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 * L_11 = (Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 *)L_11);
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_13;
		L_13 = ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tD58EEB7D030248D39CFED6A2AF6CF7A63FCB6A92 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m3446A33519309208065FF5431695DC1C46E0A28B_gshared (WhereSelectListIterator_2_tCEC91A59A37C4AEFB9D468DB0EAF8C7E18C617DD * __this, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 * L_1 = (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *, RuntimeObject*, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCF313A191371C8CCC2E79D89A3BF21714EFDB20E_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m667BCD94E83BB3A02AF2D66E07B089FA86971342_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * L_3 = (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mEE0E8B173345B059100E0736D106FFAE0C2D29CA_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mAC87184664F73DD7F3EC4AB4CE2BDE71BE76249D_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m10DEC060B53818B1458E4964B5DE61F270573950_gshared (WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		((  void (*) (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.Vector3>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF * WhereSelectListIterator_2_Clone_mB44BBE2A2C3D71CC154BD4A7FB2FDDBD1BFDD2CD_gshared (WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A * L_2 = (Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A *)__this->get_selector_5();
		WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1 * L_3 = (WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.Vector3>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mDC473EAB353D9929D2F5B789504AB791A08183BF_gshared (WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4;
		L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6;
		L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A * L_11 = (Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A *)L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t3BAEFBC83B1F104F246CC17B449E3775D5E6881A *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>::Dispose() */, (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,UnityEngine.Vector3>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mF794FDDCEF03B476B7598BE710C4B069B8D0FC30_gshared (WhereSelectListIterator_2_t29CA3510DC453EF517F074F3FAD3F739424123C1 * __this, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 * L_1 = (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *, RuntimeObject*, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.ProBuilder.Edge>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m6D25A6CA48CB6F36350C78A17B4EDDAC32E6D0C1_gshared (WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363 * __this, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source0, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate1, Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		((  void (*) (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.ProBuilder.Edge>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 * WhereSelectListIterator_2_Clone_m155DFC5FCB66D5116F4894C59F76AE1E62B1FF4B_gshared (WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363 * __this, const RuntimeMethod* method)
{
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E * L_2 = (Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E *)__this->get_selector_5();
		WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363 * L_3 = (WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363 *, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_0, (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_1, (Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.ProBuilder.Edge>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m8AB8CB916540E167C5913F3C1A94D9F284867A5B_gshared (WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_3 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		NullCheck((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3);
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  L_4;
		L_4 = ((  Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  (*) (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_5 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_6;
		L_6 = Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_inline((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_6;
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_7 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_8 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_9 = V_1;
		NullCheck((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E * L_11 = (Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E *)__this->get_selector_5();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_12 = V_1;
		NullCheck((Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E *)L_11);
		Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  L_13;
		L_13 = ((  Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  (*) (Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t040047B767A08F4BA8B3130F802087F04FB6DC2E *)L_11, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_14 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m4B93FB44F564B9B972BD3FFC30EBA3922B766501((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>::Dispose() */, (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.ProBuilder.Edge>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mA35125B78FF208A3B6A6C7EE18F9EE74F14E70E2_gshared (WhereSelectListIterator_2_tB98BCFF1AD2E52847FAD24C5F4F89F0F8B4EC363 * __this, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 * L_1 = (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *, RuntimeObject*, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mE519A0E8D91749C3D31C3CBE424C4EAC180504E3_gshared (WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6 * __this, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source0, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate1, Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_m83C7436B8FC8E30BC8F21D87EA6B42B4001CC82B_gshared (WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6 * __this, const RuntimeMethod* method)
{
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 * L_2 = (Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 *)__this->get_selector_5();
		WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6 * L_3 = (WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6 *, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_0, (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_1, (Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m65E5EF23C4194C6AB1896EBB3669BCBD199715B7_gshared (WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_3 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		NullCheck((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3);
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  L_4;
		L_4 = ((  Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  (*) (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_5 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_6;
		L_6 = Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_inline((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_6;
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_7 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_8 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_9 = V_1;
		NullCheck((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 * L_11 = (Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 *)__this->get_selector_5();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_12 = V_1;
		NullCheck((Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tB9E8AD26B8D0CF7A0930536D3B3948BD3B178EF1 *)L_11, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_14 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m4B93FB44F564B9B972BD3FFC30EBA3922B766501((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m26EFA79C0F86A9104E6632A7274477D4287348F1_gshared (WhereSelectListIterator_2_tE43D115CD9912D509C6740940021447E3254EBC6 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m8DE39711C20462F53C3ADD708C9E5EC6330D86DF_gshared (WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127 * __this, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source0, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate1, Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		((  void (*) (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB * WhereSelectListIterator_2_Clone_mDB119ECA229E22998FA35A9F636D88A985FDB358_gshared (WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127 * __this, const RuntimeMethod* method)
{
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 * L_2 = (Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 *)__this->get_selector_5();
		WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127 * L_3 = (WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127 *, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_0, (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_1, (Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mB84ED465D101C2100804ACC1D06A428EBF7715B8_gshared (WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_3 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		NullCheck((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3);
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  L_4;
		L_4 = ((  Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  (*) (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_5 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_6;
		L_6 = Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_inline((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_6;
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_7 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_8 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_9 = V_1;
		NullCheck((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 * L_11 = (Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 *)__this->get_selector_5();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_12 = V_1;
		NullCheck((Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 *)L_11);
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_13;
		L_13 = ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t794B0A9B10ED0580244B6811CF7CFA5745FC27F3 *)L_11, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_14 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m4B93FB44F564B9B972BD3FFC30EBA3922B766501((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m6FEB123B503607370AAF566D8A63C4A6C30BE2B9_gshared (WhereSelectListIterator_2_t2B0C2D13BFCB3B718FFDA45A42A91F1142E69127 * __this, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 * L_1 = (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *, RuntimeObject*, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m13AFFAB2BF35338ACC5239D7A54777FECA399F2A_gshared (WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F * __this, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source0, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate1, Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m799BD2FE7773A38168A0D8AABF911C5BB6F29370_gshared (WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F * __this, const RuntimeMethod* method)
{
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 * L_2 = (Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 *)__this->get_selector_5();
		WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F * L_3 = (WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F *, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_0, (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_1, (Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m4327E543FF2BF886DC23AE0062BE0B25F17C9233_gshared (WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_3 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		NullCheck((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3);
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  L_4;
		L_4 = ((  Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  (*) (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_5 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_6;
		L_6 = Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_inline((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_6;
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_7 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_8 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_9 = V_1;
		NullCheck((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 * L_11 = (Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 *)__this->get_selector_5();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_12 = V_1;
		NullCheck((Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t005E8EF5E159894D8474A3B2F511313000B11680 *)L_11, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_14 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m4B93FB44F564B9B972BD3FFC30EBA3922B766501((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mE703E433BB66ADFE3E9B5C59FE3A6668453C67BF_gshared (WhereSelectListIterator_2_t38CF15C7FD181325DE4DDCA7C64001394FF03E2F * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m7B2918E49DBAAD7EA108EEC260C62F1716A97EB1_gshared (WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529 * __this, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * ___source0, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * ___predicate1, Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		((  void (*) (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.Vector3>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF * WhereSelectListIterator_2_Clone_mAC584530781EC76AEAAA37014488B74784469AF1_gshared (WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529 * __this, const RuntimeMethod* method)
{
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_0 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_1 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 * L_2 = (Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 *)__this->get_selector_5();
		WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529 * L_3 = (WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529 *, List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_0, (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_1, (Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.Vector3>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m6B5C4A8E9C93B751295EDE6E60D517E0DB8142C4_gshared (WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 * L_3 = (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)__this->get_source_3();
		NullCheck((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3);
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  L_4;
		L_4 = ((  Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE  (*) (List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tBBBE3B7C647783F39A3A69F2DEDAB0E78CAD52E5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_5 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_6;
		L_6 = Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_inline((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_6;
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_7 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 * L_8 = (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)__this->get_predicate_4();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_9 = V_1;
		NullCheck((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t914F2382286698ABCF7602CD671A49AD142738A8 *)L_8, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 * L_11 = (Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 *)__this->get_selector_5();
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_12 = V_1;
		NullCheck((Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 *)L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 *, Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA8CE0E244187C8C34F23D67E9925B51E4317D2A3 *)L_11, (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * L_14 = (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m4B93FB44F564B9B972BD3FFC30EBA3922B766501((Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)(Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>::Dispose() */, (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.Substring,UnityEngine.Vector3>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mBF4FA3A21E607DD75DECFD1961983C9B5E7BE4B0_gshared (WhereSelectListIterator_2_t3677AEC1A9BA6D9C933CBEFF5D9F92B4A9D87529 * __this, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 * L_1 = (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *, RuntimeObject*, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.ProBuilder.Edge>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m403C13E0BE6A3FB1BF6E8A0BABC93ED8F2AB6C48_gshared (WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F * __this, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source0, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate1, Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		((  void (*) (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.ProBuilder.Edge>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 * WhereSelectListIterator_2_Clone_m7207BC83E23EA2008B944AB260AEEE4978DD76E6_gshared (WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F * __this, const RuntimeMethod* method)
{
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 * L_2 = (Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 *)__this->get_selector_5();
		WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F * L_3 = (WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F *, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_0, (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_1, (Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.ProBuilder.Edge>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m9F83E49655D062685EA88679D35F10E586DF7BAE_gshared (WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_3 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		NullCheck((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3);
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  L_4;
		L_4 = ((  Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_5 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_inline((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_6;
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_7 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_8 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_1;
		NullCheck((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 * L_11 = (Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 *)__this->get_selector_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = V_1;
		NullCheck((Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 *)L_11);
		Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  L_13;
		L_13 = ((  Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  (*) (Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t88AB91BBB6E00DC11936250A0515012606223C66 *)L_11, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_14 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m7A577B0782F0D174CEA921C7B67075BD60034A6C((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>::Dispose() */, (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.ProBuilder.Edge>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mC31726C2F29364205FEAFA76951FA43834A4644E_gshared (WhereSelectListIterator_2_t1E33A29E26BE5533EEE9DD02ECD445E7CA90582F * __this, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 * L_1 = (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *, RuntimeObject*, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mE7EC7B289FDA952C72D460BA2C97ABE468C1CBE3_gshared (WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D * __this, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source0, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate1, Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_m20C33C36E9E89E245FC306AF385EC8162453A391_gshared (WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D * __this, const RuntimeMethod* method)
{
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 * L_2 = (Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 *)__this->get_selector_5();
		WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D * L_3 = (WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D *, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_0, (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_1, (Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mB2B59AC5674DBD0ABAD9603444EC3E2AF8652A35_gshared (WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_3 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		NullCheck((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3);
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  L_4;
		L_4 = ((  Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_5 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_inline((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_6;
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_7 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_8 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_1;
		NullCheck((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 * L_11 = (Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 *)__this->get_selector_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = V_1;
		NullCheck((Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tD76164019E9490B0D154CC9F0F356F88C082E522 *)L_11, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_14 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m7A577B0782F0D174CEA921C7B67075BD60034A6C((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m9B802E44C9C52A54A0142EE41FBEA5442C7D72AF_gshared (WhereSelectListIterator_2_t689D070B43D147A9E7E89DCA2BF16EF1C6C8985D * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCBADE30FECB5C2787549EBBE45FAA72CC15D552B_gshared (WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07 * __this, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source0, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate1, Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		((  void (*) (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB * WhereSelectListIterator_2_Clone_mB0B2AEE774CE608E58934C81352FDC8756B2644F_gshared (WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07 * __this, const RuntimeMethod* method)
{
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 * L_2 = (Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 *)__this->get_selector_5();
		WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07 * L_3 = (WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07 *, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_0, (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_1, (Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m384C96E8AA75DD6CF2A671F7702B0E246E313181_gshared (WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_3 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		NullCheck((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3);
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  L_4;
		L_4 = ((  Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_5 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_inline((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_6;
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_7 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_8 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_1;
		NullCheck((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 * L_11 = (Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 *)__this->get_selector_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = V_1;
		NullCheck((Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 *)L_11);
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_13;
		L_13 = ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA033AD1BCE0B2CDD57F70F5142F09873D5CEBE40 *)L_11, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_14 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m7A577B0782F0D174CEA921C7B67075BD60034A6C((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m7E678C4F8EA18DBA8078B3DBF4D87637F0965C3D_gshared (WhereSelectListIterator_2_tE6D77018C2913084AE1536608DA114AF6AAF8A07 * __this, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 * L_1 = (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *, RuntimeObject*, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m6B20207923E935E09507554267682FFB1FA9134C_gshared (WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5 * __this, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source0, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate1, Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m310D6F290E890F24F0CE98FF358730356D2E5A53_gshared (WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5 * __this, const RuntimeMethod* method)
{
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 * L_2 = (Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 *)__this->get_selector_5();
		WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5 * L_3 = (WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5 *, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_0, (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_1, (Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mF7A10416C10E620C1C5AFFB19F9BF3B479236B5E_gshared (WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_3 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		NullCheck((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3);
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  L_4;
		L_4 = ((  Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_5 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_inline((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_6;
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_7 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_8 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_1;
		NullCheck((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 * L_11 = (Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 *)__this->get_selector_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = V_1;
		NullCheck((Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t28874FFEA74372511D8F3BBAEAE93B9799588020 *)L_11, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_14 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m7A577B0782F0D174CEA921C7B67075BD60034A6C((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mFF542356BD90CBBCFCEBA3F5EEA3051AB044003E_gshared (WhereSelectListIterator_2_t38EB87FE90AA357AD2708B1EE42CD959F39312A5 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mA3EA2B8DAFA9195A91DC2FBC77349262DA58DE31_gshared (WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838 * __this, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * ___source0, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate1, Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		((  void (*) (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.Vector3>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF * WhereSelectListIterator_2_Clone_m7293F3440C6504E4F63BF1AD868EF6764A80DA2B_gshared (WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838 * __this, const RuntimeMethod* method)
{
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_0 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_1 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B * L_2 = (Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B *)__this->get_selector_5();
		WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838 * L_3 = (WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838 *, List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_0, (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_1, (Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.Vector3>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m5B5DED5E1D8C1DFECB33D7CAC88772D1422017BA_gshared (WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 * L_3 = (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)__this->get_source_3();
		NullCheck((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3);
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  L_4;
		L_4 = ((  Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68  (*) (List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t400048180333F4A09A4A727C9A666AA5D2BB27A9 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_5 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_inline((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_6;
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_7 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_8 = (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)__this->get_predicate_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_1;
		NullCheck((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_8, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B * L_11 = (Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B *)__this->get_selector_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = V_1;
		NullCheck((Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B *)L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t8773E619330600312B6BC5AAEFF1B20ECCD0028B *)L_11, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * L_14 = (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m7A577B0782F0D174CEA921C7B67075BD60034A6C((Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)(Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>::Dispose() */, (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector2,UnityEngine.Vector3>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m81A74741A97D69D9C3E8046E16D5C6DFDF25D529_gshared (WhereSelectListIterator_2_t9A0897D89E86988F363BFD903B2F0C6539856838 * __this, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 * L_1 = (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *, RuntimeObject*, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m6735D6F95D5F585A0DD4CEBCF4FE8B04C32EB0AB_gshared (WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645 * __this, List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___source0, Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * ___predicate1, Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mB1F812294ACE65C6E916915A8843DA05B9DF7C71_gshared (WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645 * __this, const RuntimeMethod* method)
{
	{
		List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * L_0 = (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)__this->get_source_3();
		Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * L_1 = (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)__this->get_predicate_4();
		Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 * L_2 = (Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 *)__this->get_selector_5();
		WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645 * L_3 = (WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645 *, List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *, Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *, Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)L_0, (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)L_1, (Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m3E9D6C95381010D125D7F5378536EB7B6C3424EC_gshared (WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * L_3 = (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)__this->get_source_3();
		NullCheck((List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)L_3);
		Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  L_4;
		L_4 = ((  Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  (*) (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * L_5 = (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)__this->get_address_of_enumerator_6();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_6;
		L_6 = Enumerator_get_Current_m32439407464C10970CC963587D0D2E0DD861ED9B_inline((Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_6;
		Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * L_7 = (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * L_8 = (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)__this->get_predicate_4();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_9 = V_1;
		NullCheck((Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)L_8, (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 * L_11 = (Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 *)__this->get_selector_5();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_12 = V_1;
		NullCheck((Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 *, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE546897E3EFEFCF00DB44807E68C5DEF237A2071 *)L_11, (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * L_14 = (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mF378337D9CA43F03755456627ECC0436E94A8B9C((Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m242E66A7BA03EB4143D1BF7D0BEDA70EE19201E4_gshared (WhereSelectListIterator_2_tEA6B38BEA5792D58F802559CDF6067261B0F7645 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mA7D0CC66CE654E9C55D615B7B36FFDB0A40CEE37_gshared (WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C * __this, List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___source0, Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * ___predicate1, Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *)__this);
		((  void (*) (Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,UnityEngine.Vector2>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF * WhereSelectListIterator_2_Clone_mCEAE700DB4B782CBFB41E6A24790F02ABF7B977A_gshared (WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C * __this, const RuntimeMethod* method)
{
	{
		List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * L_0 = (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)__this->get_source_3();
		Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * L_1 = (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)__this->get_predicate_4();
		Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 * L_2 = (Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 *)__this->get_selector_5();
		WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C * L_3 = (WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C *, List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *, Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *, Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)L_0, (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)L_1, (Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,UnityEngine.Vector2>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m530AD95D6D3E6870B1DF37D32004BFF1850AC352_gshared (WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * L_3 = (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)__this->get_source_3();
		NullCheck((List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)L_3);
		Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  L_4;
		L_4 = ((  Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781  (*) (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * L_5 = (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)__this->get_address_of_enumerator_6();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_6;
		L_6 = Enumerator_get_Current_m32439407464C10970CC963587D0D2E0DD861ED9B_inline((Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_6;
		Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * L_7 = (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 * L_8 = (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)__this->get_predicate_4();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_9 = V_1;
		NullCheck((Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t06E4D1F7A5FCD6F9551A7741FF459CE87D43C148 *)L_8, (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 * L_11 = (Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 *)__this->get_selector_5();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_12 = V_1;
		NullCheck((Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 *)L_11);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13;
		L_13 = ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 *, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t3C82644A98345A1283AC8EA5C07A6C505404A7D1 *)L_11, (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * L_14 = (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_mF378337D9CA43F03755456627ECC0436E94A8B9C((Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)(Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector2>::Dispose() */, (Iterator_1_tC65CA16F3F1C7E3921BFD2B03AC69FE5BF6996BF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Vector4,UnityEngine.Vector2>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mDC0C113D11D28FCB011FB0D3C4623684882F961A_gshared (WhereSelectListIterator_2_t601C7D0CDDABF88C6F98DA7E13B6AEA300928D0C * __this, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tA5C39F085357986D0D6ABAC117C73C8BAE320A2C * L_1 = (WhereEnumerableIterator_1_tA5C39F085357986D0D6ABAC117C73C8BAE320A2C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tA5C39F085357986D0D6ABAC117C73C8BAE320A2C *, RuntimeObject*, Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t98EC33D5C203218F10A98B08E709DBFB2353DB9A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.ProBuilder.Edge>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m3D6D3C53791A18E663C731725051F8519CAD238E_gshared (WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B * __this, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source0, Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate1, Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		((  void (*) (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.ProBuilder.Edge>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 * WhereSelectListIterator_2_Clone_mA36C898728E7913B9AF10726454A944C3B09D3D3_gshared (WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B * __this, const RuntimeMethod* method)
{
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 * L_2 = (Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 *)__this->get_selector_5();
		WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B * L_3 = (WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B *, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, Func_2_tF3B895913B44A5233F386097973299392788EA81 *, Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_0, (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_1, (Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.ProBuilder.Edge>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mF358EA7E5D6B0E7EB8A39EB3C81DB7F4BE0CE2F6_gshared (WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_3 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		NullCheck((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3);
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  L_4;
		L_4 = ((  Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  (*) (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_5 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_6;
		L_6 = Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_inline((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_6;
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_7 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_8 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_9 = V_1;
		NullCheck((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tF3B895913B44A5233F386097973299392788EA81 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 * L_11 = (Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 *)__this->get_selector_5();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_12 = V_1;
		NullCheck((Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 *)L_11);
		Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  L_13;
		L_13 = ((  Edge_t59DC4A6451BF45A4BB245CB1D084FCD1E447EF4D  (*) (Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5A65335AD39B4402E4522F23CAB17DAD4B64B978 *)L_11, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_14 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m262885D7B2625EA9D3DB985226C942342C65B3F7((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.ProBuilder.Edge>::Dispose() */, (Iterator_1_tD26254F9AE3CE7A7FC78FFF64D2D833065C0CF46 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.ProBuilder.Edge>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mAAE13383A6A7102F0F8FEC185CEA9E5B97947556_gshared (WhereSelectListIterator_2_t8FD16770E0FDB6C79B5FAE18F3C3D9CCCD4C606B * __this, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 * L_1 = (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC395630E7DEF22EE03BA062F331A653DACCC7D49 *, RuntimeObject*, Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t9FE23923D8F6B47DBC95985FCC01153718259F1F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m8CC0A045EE495301E11DE340D6068C1931E2250A_gshared (WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1 * __this, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source0, Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate1, Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mC263FD86D37C8DB818B55132B79FB468EC319E76_gshared (WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1 * __this, const RuntimeMethod* method)
{
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 * L_2 = (Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 *)__this->get_selector_5();
		WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1 * L_3 = (WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1 *, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, Func_2_tF3B895913B44A5233F386097973299392788EA81 *, Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_0, (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_1, (Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m2973E8A74B8228D8DD605CAE8EE93D40240EC817_gshared (WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_3 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		NullCheck((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3);
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  L_4;
		L_4 = ((  Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  (*) (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_5 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_6;
		L_6 = Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_inline((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_6;
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_7 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_8 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_9 = V_1;
		NullCheck((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tF3B895913B44A5233F386097973299392788EA81 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 * L_11 = (Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 *)__this->get_selector_5();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_12 = V_1;
		NullCheck((Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 *)L_11);
		int32_t L_13;
		L_13 = ((  int32_t (*) (Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA6CCF7CD95BB33A4226C07B15333BAE08F5B5AE8 *)L_11, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_14 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m262885D7B2625EA9D3DB985226C942342C65B3F7((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m7310223298FAA1EF806CE79B8376FA1E2D2F76F0_gshared (WhereSelectListIterator_2_t45151043CC0BFFC8925523415472FE7B3BACF3E1 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mBA2CDDDDA953ECA3707E7F02433BD36A49791567_gshared (WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21 * __this, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source0, Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate1, Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		((  void (*) (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB * WhereSelectListIterator_2_Clone_mD600ED6B20810892CDDEB1CB6585CD5B0CED05BC_gshared (WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21 * __this, const RuntimeMethod* method)
{
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 * L_2 = (Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 *)__this->get_selector_5();
		WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21 * L_3 = (WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21 *, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, Func_2_tF3B895913B44A5233F386097973299392788EA81 *, Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_0, (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_1, (Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m8AF769B6C53BDF35EEA88C1693B2138ADEAA594B_gshared (WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_3 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		NullCheck((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3);
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  L_4;
		L_4 = ((  Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  (*) (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_5 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_6;
		L_6 = Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_inline((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_6;
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_7 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_8 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_9 = V_1;
		NullCheck((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tF3B895913B44A5233F386097973299392788EA81 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 * L_11 = (Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 *)__this->get_selector_5();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_12 = V_1;
		NullCheck((Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 *)L_11);
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_13;
		L_13 = ((  InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  (*) (Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t9AE5B85137829B5CB1D8C8E4D981EE564EA4F794 *)L_11, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_14 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m262885D7B2625EA9D3DB985226C942342C65B3F7((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.InputSystem.Utilities.InternedString>::Dispose() */, (Iterator_1_t13F8315E94AB2FF47535F5872529A16C4AF9EBBB *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.InputSystem.Utilities.InternedString>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m99C8EFD68588BE28C43A6D1E6438454700F491BA_gshared (WhereSelectListIterator_2_t7B0FE4959859811D7697803C261D5F89CA804E21 * __this, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 * L_1 = (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1AE0E907554F08C4CE0C17357E93282A774A2066 *, RuntimeObject*, Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_tD7DA058C5012E4A87F1E7EACAE793DB0040D57FD *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mD9644AA0F627D94D36FF0BC98489EB37BC67E2C4_gshared (WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A * __this, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source0, Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate1, Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mFD1C57BE13C598DFF2928021AB01B8402481AD5D_gshared (WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A * __this, const RuntimeMethod* method)
{
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC * L_2 = (Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC *)__this->get_selector_5();
		WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A * L_3 = (WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A *, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, Func_2_tF3B895913B44A5233F386097973299392788EA81 *, Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_0, (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_1, (Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m654ADF799003BFF1B12856BDC327CEED2FEA3EE7_gshared (WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_3 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		NullCheck((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3);
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  L_4;
		L_4 = ((  Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  (*) (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_5 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_6;
		L_6 = Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_inline((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_6;
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_7 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_8 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_9 = V_1;
		NullCheck((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tF3B895913B44A5233F386097973299392788EA81 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC * L_11 = (Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC *)__this->get_selector_5();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_12 = V_1;
		NullCheck((Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC *)L_11);
		RuntimeObject * L_13;
		L_13 = ((  RuntimeObject * (*) (Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7A1E4C1615240DAC38A61126438E687A3D6941BC *)L_11, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_14 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m262885D7B2625EA9D3DB985226C942342C65B3F7((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m143B62A3624E848688C31DD4F4950094664984D8_gshared (WhereSelectListIterator_2_tE6E3E17AE3FF9B661DAF4CB47E4959CD94D5896A * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m168466D0CADC87276B6BFA13774F5686EE07885E_gshared (WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC * __this, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * ___source0, Func_2_tF3B895913B44A5233F386097973299392788EA81 * ___predicate1, Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		((  void (*) (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.Vector3>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF * WhereSelectListIterator_2_Clone_mEEAE0BF461695DA44BD4D9C74CF9A338E630958E_gshared (WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC * __this, const RuntimeMethod* method)
{
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_0 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_1 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 * L_2 = (Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 *)__this->get_selector_5();
		WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC * L_3 = (WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC *, List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, Func_2_tF3B895913B44A5233F386097973299392788EA81 *, Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_0, (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_1, (Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)L_3;
	}
}
// System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.Vector3>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m0B9995F160419251618C3CCB16481ECCD7EF46BB_gshared (WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E * L_3 = (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)__this->get_source_3();
		NullCheck((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3);
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  L_4;
		L_4 = ((  Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5  (*) (List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC2452E93E5B8E31149932C482B9B7286089CB38E *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_5 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_6;
		L_6 = Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_inline((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_6;
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_7 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tF3B895913B44A5233F386097973299392788EA81 * L_8 = (Func_2_tF3B895913B44A5233F386097973299392788EA81 *)__this->get_predicate_4();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_9 = V_1;
		NullCheck((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8);
		bool L_10;
		L_10 = ((  bool (*) (Func_2_tF3B895913B44A5233F386097973299392788EA81 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tF3B895913B44A5233F386097973299392788EA81 *)L_8, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 * L_11 = (Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 *)__this->get_selector_5();
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_12 = V_1;
		NullCheck((Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 *)L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 *, JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tB4141711975FE68FABE6BD94C25AD3498A0D6967 *)L_11, (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * L_14 = (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)__this->get_address_of_enumerator_6();
		bool L_15;
		L_15 = Enumerator_MoveNext_m262885D7B2625EA9D3DB985226C942342C65B3F7((Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)(Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<UnityEngine.Vector3>::Dispose() */, (Iterator_1_t04F5D870FD247BBBEE27254587FA10F440D4EEFF *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.InputSystem.Utilities.JsonParser/JsonValue,UnityEngine.Vector3>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m21E39F5EDFD3083CBB12986DE0D4BA16A205A957_gshared (WhereSelectListIterator_2_t942F8B8B84A249FCE9F1AD39D9CB7F4D42A2ECBC * __this, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 * L_1 = (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t0E01F06572EA26BE9E79530811037753CF6B3BF8 *, RuntimeObject*, Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t3041FD3183D19FE8416AE2E43A6398B2C06B7269 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return (RuntimeObject*)L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  Enumerator_get_Current_m5D58E98BBF2B7DFDA87005EACBA952CF220E8EAC_gshared_inline (Enumerator_t386DBB3F2F8C4C77206A826C9376FDD81D984455 * __this, const RuntimeMethod* method)
{
	{
		EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A  L_0 = (EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A )__this->get_current_3();
		return (EdgeLookup_tBB3FEB19F640D730C4E3A0EBB92513E9861FC79A )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  Enumerator_get_Current_m8FAEAA663EBD7E2719BB9F08490A5400E52BEFB9_gshared_inline (Enumerator_tD0B60894B040FDF61553631F59BD4D8B0F9B98F3 * __this, const RuntimeMethod* method)
{
	{
		InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  L_0 = (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )__this->get_current_3();
		return (InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  Enumerator_get_Current_m438E410457432710451CCC26CB94B5A0D5481607_gshared_inline (Enumerator_tB3A134E786FF712870FA78F849DB2D7E9D4CBBA4 * __this, const RuntimeMethod* method)
{
	{
		NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76  L_0 = (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )__this->get_current_3();
		return (NameAndParameters_tEBC11C9D51435C0932FBCF5076DE970B4A71EC76 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  Enumerator_get_Current_m3B5CED75AA25B19C124191D71FA1A668BEB42598_gshared_inline (Enumerator_tE777A434BDE729292D8409A32B903BFEAF9383CE * __this, const RuntimeMethod* method)
{
	{
		NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94  L_0 = (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )__this->get_current_3();
		return (NamedValue_tE0B0EA747A0E5B3A8B18EA5AD69BB7F7F91D1B94 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  Enumerator_get_Current_m5AA44575600666864F029E09BB376DDE7226BDEF_gshared_inline (Enumerator_tCE2D954F8959E8B8F64E48F1348154CBCC288CDE * __this, const RuntimeMethod* method)
{
	{
		Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815  L_0 = (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )__this->get_current_3();
		return (Substring_t9AD8D12A00743C9AF2A3E122F51B06CCE4615815 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Enumerator_get_Current_mFA4853D063467DAFFBDF729420AF7FE91BD77160_gshared_inline (Enumerator_tA0E560D0AB839E2C4C1012238EA327E24F1A1E68 * __this, const RuntimeMethod* method)
{
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )__this->get_current_3();
		return (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  Enumerator_get_Current_m32439407464C10970CC963587D0D2E0DD861ED9B_gshared_inline (Enumerator_tF6B201E7214E46815E24879BFB75303CF3649781 * __this, const RuntimeMethod* method)
{
	{
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_0 = (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )__this->get_current_3();
		return (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  Enumerator_get_Current_m2CA4A39F2A1314A05F12333B1FA769D174943A24_gshared_inline (Enumerator_tA90A1F128B418F088DA972BE021AF1C339C6A8D5 * __this, const RuntimeMethod* method)
{
	{
		JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F  L_0 = (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )__this->get_current_3();
		return (JsonValue_t4A09AABA8A49BE4FD4182C9438D340BC8F38831F )L_0;
	}
}
