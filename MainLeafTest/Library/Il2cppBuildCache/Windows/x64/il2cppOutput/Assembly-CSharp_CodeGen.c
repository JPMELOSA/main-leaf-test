﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Box::Start()
extern void Box_Start_m303F32963DB378B8D1424990FB77B8667550EB60 (void);
// 0x00000002 System.Void Box::Update()
extern void Box_Update_mD1ED2D1DFF41711B93C8FF7B26229C4185747FB6 (void);
// 0x00000003 System.Void Box::OnTriggerStay(UnityEngine.Collider)
extern void Box_OnTriggerStay_mBDCDFA843A559414FDEEA532C3995E6C4FE2BE5D (void);
// 0x00000004 System.Void Box::OnTriggerExit(UnityEngine.Collider)
extern void Box_OnTriggerExit_mB8E4803C99C5BA92EE0B95C6C83D6025A8E7AA8D (void);
// 0x00000005 System.Void Box::blockBox(System.Boolean)
extern void Box_blockBox_mFA226C2EFC5A3C7D8EC19E383618A6E24B363582 (void);
// 0x00000006 System.Void Box::.ctor()
extern void Box__ctor_mFDB4D9912FDCE47BA2A62DB77AD92F62989E0295 (void);
// 0x00000007 System.Void Crystals::Start()
extern void Crystals_Start_mD7F7114BF22B5D849766B4BEC0358AC581B37ABB (void);
// 0x00000008 System.Void Crystals::OnTriggerEnter(UnityEngine.Collider)
extern void Crystals_OnTriggerEnter_m4A1C1A2B439914885D2C1DB36E373F5579833ACC (void);
// 0x00000009 System.Void Crystals::CrytalColor()
extern void Crystals_CrytalColor_mC3C8AE81CE0C33215000277A927BF42DA1A75EF4 (void);
// 0x0000000A System.Void Crystals::.ctor()
extern void Crystals__ctor_m70E107C9701D646121EFF210DFA2CF12F786C92E (void);
// 0x0000000B System.Void EnemyAtaque::OnTriggerEnter(UnityEngine.Collider)
extern void EnemyAtaque_OnTriggerEnter_m6FBE2261504A195151E794C74322FF0CD6CFCB1D (void);
// 0x0000000C System.Collections.IEnumerator EnemyAtaque::StopGame()
extern void EnemyAtaque_StopGame_mEB738EA1A0498564C90074A2B9DD99DC71C577B7 (void);
// 0x0000000D System.Void EnemyAtaque::.ctor()
extern void EnemyAtaque__ctor_m92BC5A96F291E83BACE739D372490ACB90FE0312 (void);
// 0x0000000E System.Void EnemyAtaque/<StopGame>d__2::.ctor(System.Int32)
extern void U3CStopGameU3Ed__2__ctor_m4D499A575C66F55CD8B598DFADF2FB48B8B37D31 (void);
// 0x0000000F System.Void EnemyAtaque/<StopGame>d__2::System.IDisposable.Dispose()
extern void U3CStopGameU3Ed__2_System_IDisposable_Dispose_m8497D9D2FF170B8F4B29BF0B4409D33D3FAE11F9 (void);
// 0x00000010 System.Boolean EnemyAtaque/<StopGame>d__2::MoveNext()
extern void U3CStopGameU3Ed__2_MoveNext_m19076221B8FB01286C8649EDBC4A044828B5A3E0 (void);
// 0x00000011 System.Object EnemyAtaque/<StopGame>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopGameU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04D778BC1EEC05C2DA6A3223241712CD4F8725D1 (void);
// 0x00000012 System.Void EnemyAtaque/<StopGame>d__2::System.Collections.IEnumerator.Reset()
extern void U3CStopGameU3Ed__2_System_Collections_IEnumerator_Reset_m09BEED7E46DAF001D5B7890A9DE5A5A90FBD100D (void);
// 0x00000013 System.Object EnemyAtaque/<StopGame>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CStopGameU3Ed__2_System_Collections_IEnumerator_get_Current_mA84A9697F2DE0221C6DD4BC3ACE48A890E9D227C (void);
// 0x00000014 System.Void EnemyMoviment::Start()
extern void EnemyMoviment_Start_m70E58D9549C8BA5938152E6294A3EBFEC5A7017D (void);
// 0x00000015 System.Void EnemyMoviment::Patrol()
extern void EnemyMoviment_Patrol_mC320985F7F896F6E56907A42BF619D3C48F716E5 (void);
// 0x00000016 System.Void EnemyMoviment::OnTriggerEnter(UnityEngine.Collider)
extern void EnemyMoviment_OnTriggerEnter_m34E675C6A37B460217125F9384338567BFC9EBAF (void);
// 0x00000017 System.Collections.IEnumerator EnemyMoviment::PauseBetweenPoints(System.Int32)
extern void EnemyMoviment_PauseBetweenPoints_m5A9A7B93BB7611082D8952F71FF0DDC3571B0040 (void);
// 0x00000018 System.Void EnemyMoviment::.ctor()
extern void EnemyMoviment__ctor_m8325A2F2261778D22F869016FB67A95E27489B0F (void);
// 0x00000019 System.Void EnemyMoviment/<PauseBetweenPoints>d__10::.ctor(System.Int32)
extern void U3CPauseBetweenPointsU3Ed__10__ctor_mC9DFE8F507DD0C33010606A2C385414FE798DD6A (void);
// 0x0000001A System.Void EnemyMoviment/<PauseBetweenPoints>d__10::System.IDisposable.Dispose()
extern void U3CPauseBetweenPointsU3Ed__10_System_IDisposable_Dispose_m960218053B90C48770DDC01503EEDB9FD015879D (void);
// 0x0000001B System.Boolean EnemyMoviment/<PauseBetweenPoints>d__10::MoveNext()
extern void U3CPauseBetweenPointsU3Ed__10_MoveNext_mCBE82EC972DF17654361740A1A5A6A479C043EAD (void);
// 0x0000001C System.Object EnemyMoviment/<PauseBetweenPoints>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPauseBetweenPointsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1251E5DA1AF26EBD3F43AC4BF23C71B1B08C879 (void);
// 0x0000001D System.Void EnemyMoviment/<PauseBetweenPoints>d__10::System.Collections.IEnumerator.Reset()
extern void U3CPauseBetweenPointsU3Ed__10_System_Collections_IEnumerator_Reset_m5D9150FCD705A629A3F83274A634DE94E36A8CDF (void);
// 0x0000001E System.Object EnemyMoviment/<PauseBetweenPoints>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CPauseBetweenPointsU3Ed__10_System_Collections_IEnumerator_get_Current_m987541499C36FEC2822C0186DE09A156EF51CC12 (void);
// 0x0000001F System.Void AudioManager::OnEnable()
extern void AudioManager_OnEnable_m680B36494B3F0E537147056468EC66D1EFCAE97E (void);
// 0x00000020 System.Void AudioManager::Start()
extern void AudioManager_Start_m54C0A7ACBAB2F38052C6B900BBBC3261339662FC (void);
// 0x00000021 System.Void AudioManager::PlayMusic(System.String)
extern void AudioManager_PlayMusic_mB50D212EBE632BF1E95380A7F222B043A0FBFFF1 (void);
// 0x00000022 System.Void AudioManager::PlaySFX(System.String)
extern void AudioManager_PlaySFX_m13FCA19E8F5B2A013718669280D1AABFB4767E03 (void);
// 0x00000023 System.Void AudioManager::.ctor()
extern void AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD (void);
// 0x00000024 System.Void GameManager::CheckScene()
extern void GameManager_CheckScene_m5D4F9FB5CDE6F44C5078C7846C85B6C24626E652 (void);
// 0x00000025 System.Void GameManager::OnTriggerStay(UnityEngine.Collider)
extern void GameManager_OnTriggerStay_m7CEBD224229F8B4E751CA0FB69458E32966EF75C (void);
// 0x00000026 System.Void GameManager::OnTriggerExit(UnityEngine.Collider)
extern void GameManager_OnTriggerExit_mCD220F68CA8D6AEDEBCAFE5603282702C9836AF4 (void);
// 0x00000027 System.Collections.IEnumerator GameManager::CrawlTime()
extern void GameManager_CrawlTime_m5BE2E577B1F4E6F00F9673438AE71EF756937476 (void);
// 0x00000028 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000029 System.Void GameManager/<CrawlTime>d__4::.ctor(System.Int32)
extern void U3CCrawlTimeU3Ed__4__ctor_mB14A3CCF90439950E906F62AC8F6297A55BDC7E9 (void);
// 0x0000002A System.Void GameManager/<CrawlTime>d__4::System.IDisposable.Dispose()
extern void U3CCrawlTimeU3Ed__4_System_IDisposable_Dispose_mAB5809BACD0B158957AE0DDA7CF1D5636D198724 (void);
// 0x0000002B System.Boolean GameManager/<CrawlTime>d__4::MoveNext()
extern void U3CCrawlTimeU3Ed__4_MoveNext_m49379FAC79F1F1B541EDD1A44041F9E6A08B0526 (void);
// 0x0000002C System.Object GameManager/<CrawlTime>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCrawlTimeU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B103A77D4FE6CEDDB1CB6D6B438E96BD396F624 (void);
// 0x0000002D System.Void GameManager/<CrawlTime>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCrawlTimeU3Ed__4_System_Collections_IEnumerator_Reset_mEFE39E70A81532CBA5C3664B6EC85E3DB0324D47 (void);
// 0x0000002E System.Object GameManager/<CrawlTime>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCrawlTimeU3Ed__4_System_Collections_IEnumerator_get_Current_m3C1903549C7EB63B415FB05B170CB3441AC421C3 (void);
// 0x0000002F System.Void MainMenu::Awake()
extern void MainMenu_Awake_mA8CB83E7D49CE72C6D0DE5D7D6313F8305A1A4FE (void);
// 0x00000030 System.Void MainMenu::Start()
extern void MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621 (void);
// 0x00000031 System.Void MainMenu::Update()
extern void MainMenu_Update_m2DE1F2570AFBF09401821F2F89779639CD0BBC76 (void);
// 0x00000032 System.Void MainMenu::PlayGame()
extern void MainMenu_PlayGame_m96A3CE2743BCB00B665AA3AC575AE4EBD9ED40B0 (void);
// 0x00000033 System.Void MainMenu::QuitGame()
extern void MainMenu_QuitGame_m9F32E266C6F6CE345067D062258362159D267030 (void);
// 0x00000034 System.Void MainMenu::PauseGame(System.Boolean)
extern void MainMenu_PauseGame_m5D489FBBACBA848C505A9431F931C16BB040A7F7 (void);
// 0x00000035 System.Void MainMenu::CheckPause()
extern void MainMenu_CheckPause_m54274FDDC3E0825E4BF94007091245D67C5BBD72 (void);
// 0x00000036 System.Void MainMenu::activeMenuGameOver()
extern void MainMenu_activeMenuGameOver_m6E930CB0636B9F8EF4D0B1844B020E26680876DC (void);
// 0x00000037 System.Collections.IEnumerator MainMenu::SplashScene()
extern void MainMenu_SplashScene_m460723A3114771E99FD30F84C2BC53CAFE735FB8 (void);
// 0x00000038 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x00000039 System.Void MainMenu/<SplashScene>d__11::.ctor(System.Int32)
extern void U3CSplashSceneU3Ed__11__ctor_mBD6EB5786D88BB9EC78E63DA512AAF3C6B42CE9C (void);
// 0x0000003A System.Void MainMenu/<SplashScene>d__11::System.IDisposable.Dispose()
extern void U3CSplashSceneU3Ed__11_System_IDisposable_Dispose_m441B5691D8AFEED842A014E366561100A8B629AA (void);
// 0x0000003B System.Boolean MainMenu/<SplashScene>d__11::MoveNext()
extern void U3CSplashSceneU3Ed__11_MoveNext_m1B8ED61E1DC9B157045FCBC426B3A89B6CA47821 (void);
// 0x0000003C System.Object MainMenu/<SplashScene>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSplashSceneU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB8FFDE2927071EAD69DAD727F25B4B9854840CE (void);
// 0x0000003D System.Void MainMenu/<SplashScene>d__11::System.Collections.IEnumerator.Reset()
extern void U3CSplashSceneU3Ed__11_System_Collections_IEnumerator_Reset_mFAE32AEBF64F666CA4DC59D60EC978554E413667 (void);
// 0x0000003E System.Object MainMenu/<SplashScene>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CSplashSceneU3Ed__11_System_Collections_IEnumerator_get_Current_mA57B369812F6C456C250B031ED907F1BCAE3BA9B (void);
// 0x0000003F System.Void ScoreManager::Awake()
extern void ScoreManager_Awake_mB6C30C958421EED1082D2D5B24532F2548DB4575 (void);
// 0x00000040 System.Void ScoreManager::GetPoints(System.Int32)
extern void ScoreManager_GetPoints_m51748B35A7A793CC843753B2A90B6DA6EBC77026 (void);
// 0x00000041 System.Void ScoreManager::UpdateScoreUI()
extern void ScoreManager_UpdateScoreUI_mF6CBF4DD6A6228B7F2CA86F58C4BBB491EA3D3C6 (void);
// 0x00000042 System.Void ScoreManager::.ctor()
extern void ScoreManager__ctor_m638A240D34643E8AB9D17553622C1C9354348354 (void);
// 0x00000043 System.Void UIManager::Awake()
extern void UIManager_Awake_mCED93604270B1E209B4E0D32F6A26DDC5AB06E30 (void);
// 0x00000044 System.Void UIManager::changeText(System.String)
extern void UIManager_changeText_mD82273B90CC2E903B4A24BF5B6F4E3D4BC63AD69 (void);
// 0x00000045 System.Void UIManager::.ctor()
extern void UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B (void);
// 0x00000046 System.Void CamSceneOne::LateUpdate()
extern void CamSceneOne_LateUpdate_mB71C3FD11B2E30C2D62BF1247947E9716BAC9A55 (void);
// 0x00000047 System.Void CamSceneOne::cameraMovement()
extern void CamSceneOne_cameraMovement_m1EB774E19A4EE7D75B8AC11E6A8AEF8C6C052311 (void);
// 0x00000048 System.Void CamSceneOne::.ctor()
extern void CamSceneOne__ctor_m99070A08F49CC7CAF0DD20592E652B13640CFA29 (void);
// 0x00000049 System.Void CamSceneTwo::Start()
extern void CamSceneTwo_Start_mF65C09486D0A4E0EEF3BB1AFAB1297B1FD1AADE0 (void);
// 0x0000004A System.Void CamSceneTwo::FixedUpdate()
extern void CamSceneTwo_FixedUpdate_mED03D1FD1FC64EE9ACFBBF519332E9F0D03EB100 (void);
// 0x0000004B System.Void CamSceneTwo::follow()
extern void CamSceneTwo_follow_m857882F07ED1FA7B49A979D36C77590C9EB9D6FE (void);
// 0x0000004C System.Void CamSceneTwo::.ctor()
extern void CamSceneTwo__ctor_m0386A176B970A9F32859B4A7599EC6E09DD81C59 (void);
// 0x0000004D System.Void Movement::Start()
extern void Movement_Start_m5FA5146A031A9B13FE98F9CCD6027EB1DBA2DF4F (void);
// 0x0000004E System.Void Movement::Update()
extern void Movement_Update_m0880BACB69D5C89071A82EAB9BC17F76151B7DF1 (void);
// 0x0000004F System.Void Movement::MovementPlayer()
extern void Movement_MovementPlayer_m01B9AFE57EDEE1995CA1E35EE9D9DC554E36B4D8 (void);
// 0x00000050 System.Void Movement::OnTriggerStay(UnityEngine.Collider)
extern void Movement_OnTriggerStay_mA2C4F3EACDECE088DF2D148E58C9E393A1388B6E (void);
// 0x00000051 System.Void Movement::OnTriggerExit(UnityEngine.Collider)
extern void Movement_OnTriggerExit_m4E66F24F5C693037E14252BAB22EE3F69BEF3C41 (void);
// 0x00000052 System.Void Movement::Animations()
extern void Movement_Animations_mEA87C9BA145FDF2E848EE68804BE5DBDF5404A98 (void);
// 0x00000053 System.Collections.IEnumerator Movement::endJump()
extern void Movement_endJump_m7421F9EABF0E6675E6718831C80E805B811707FE (void);
// 0x00000054 System.Void Movement::Idle()
extern void Movement_Idle_m9F3C6CDB22589BE562FA30C205D8D8E7950DB8C7 (void);
// 0x00000055 System.Void Movement::Run()
extern void Movement_Run_m7FE2E54C885DA5DED13A3105881E78C6DA3DD390 (void);
// 0x00000056 System.Void Movement::WalkBack()
extern void Movement_WalkBack_mF171B314D5575D2EE779CECCC34E86AD4A64B9D1 (void);
// 0x00000057 System.Void Movement::Jump()
extern void Movement_Jump_mD745E7B705D37BC4F7CEF0C2A4575E5DB15EBE1C (void);
// 0x00000058 System.Void Movement::Pull()
extern void Movement_Pull_m566BA1CF89DA78074414178B601F7C6A215F852C (void);
// 0x00000059 System.Void Movement::Push()
extern void Movement_Push_m3A0F2220CEE2742AE70995AC06F244CBA7054384 (void);
// 0x0000005A System.Void Movement::Crawl()
extern void Movement_Crawl_m16B950AAD76EEE17EB1D6148852988C1F3E53375 (void);
// 0x0000005B System.Void Movement::Fall()
extern void Movement_Fall_m2C979047F3A8AD028694AE7D7E84374F39EF7E99 (void);
// 0x0000005C System.Void Movement::.ctor()
extern void Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB (void);
// 0x0000005D System.Void Movement/<endJump>d__16::.ctor(System.Int32)
extern void U3CendJumpU3Ed__16__ctor_m76B02F86BA012943C65ECD3DD1967DDE1DFC4E1C (void);
// 0x0000005E System.Void Movement/<endJump>d__16::System.IDisposable.Dispose()
extern void U3CendJumpU3Ed__16_System_IDisposable_Dispose_m90A5ADEB46FBA2B51FE8323140BFCFBC3C152DDB (void);
// 0x0000005F System.Boolean Movement/<endJump>d__16::MoveNext()
extern void U3CendJumpU3Ed__16_MoveNext_m7773C3A634586FFB8C818E3A8BE57DD63AD4E737 (void);
// 0x00000060 System.Object Movement/<endJump>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CendJumpU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA71A8227E7DCF926A952C7580A097102F54F62CF (void);
// 0x00000061 System.Void Movement/<endJump>d__16::System.Collections.IEnumerator.Reset()
extern void U3CendJumpU3Ed__16_System_Collections_IEnumerator_Reset_m1A02DB8D86D328ED4A27C6CA5082EE8E5D8653F3 (void);
// 0x00000062 System.Object Movement/<endJump>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CendJumpU3Ed__16_System_Collections_IEnumerator_get_Current_m884936D4F2743C7088AABED8567D6E3FB79EEB0F (void);
// 0x00000063 System.Void PlayerMove::Awake()
extern void PlayerMove_Awake_mD0B694351FB695C65CAFAD8C097849AF11391C4A (void);
// 0x00000064 System.Void PlayerMove::Start()
extern void PlayerMove_Start_m472BE983E7DC91CA752BB2994C83384C9F0DB277 (void);
// 0x00000065 System.Void PlayerMove::Update()
extern void PlayerMove_Update_m9AC3A24B5AFBD10381EFFC5A68D0DB56C73E2D90 (void);
// 0x00000066 System.Void PlayerMove::Move()
extern void PlayerMove_Move_m4614EC8657FCE8F2C9D5657085B96830F20DD306 (void);
// 0x00000067 System.Void PlayerMove::.ctor()
extern void PlayerMove__ctor_mDCA304173F32D13822B1DF8FA9F46A4446A215D9 (void);
// 0x00000068 System.Void PullingBox::Awake()
extern void PullingBox_Awake_m2AF238B3DCB605107C52FD58D2049F54B19D235D (void);
// 0x00000069 System.Void PullingBox::OnTriggerStay(UnityEngine.Collider)
extern void PullingBox_OnTriggerStay_m8C7BE204DD4B80A5069E2EEE10ECEFB2D0F018CB (void);
// 0x0000006A System.Void PullingBox::OnTriggerExit(UnityEngine.Collider)
extern void PullingBox_OnTriggerExit_mE2A01C0C1097D2E6A94D4AD93E9A70D6D219F8AB (void);
// 0x0000006B System.Void PullingBox::.ctor()
extern void PullingBox__ctor_mF036F4DD83CC24562C4099B5D15CCA987C1EC23B (void);
// 0x0000006C System.Void UnityStandardAssets.Water.WaterBasic::Update()
extern void WaterBasic_Update_m79AE15CDF47AA396823D10220EBA44C34CD6F569 (void);
// 0x0000006D System.Void UnityStandardAssets.Water.WaterBasic::.ctor()
extern void WaterBasic__ctor_mF258EB45327E0929D2CB72B5266080C3F2E9CF4A (void);
// 0x0000006E System.Void UnityStandardAssets.Water.Displace::Awake()
extern void Displace_Awake_mE99DEB129DED3AF1991C408D98A52E342B4643A7 (void);
// 0x0000006F System.Void UnityStandardAssets.Water.Displace::OnEnable()
extern void Displace_OnEnable_m952A8DDD7BF19B262520923CACCAE21927044BD8 (void);
// 0x00000070 System.Void UnityStandardAssets.Water.Displace::OnDisable()
extern void Displace_OnDisable_m344E8D36AD672D92E465CD50F28E512A817B8B08 (void);
// 0x00000071 System.Void UnityStandardAssets.Water.Displace::.ctor()
extern void Displace__ctor_mA627066AA526C79EF3AA8D4FCDF70261CEA5CFA8 (void);
// 0x00000072 System.Void UnityStandardAssets.Water.GerstnerDisplace::.ctor()
extern void GerstnerDisplace__ctor_m718281CC56902E6BF82FAC236AB3043BFA0E9BFF (void);
// 0x00000073 System.Void UnityStandardAssets.Water.MeshContainer::.ctor(UnityEngine.Mesh)
extern void MeshContainer__ctor_mF1E5D7C018B2F9F756CECB8C1A0497406F833E3B (void);
// 0x00000074 System.Void UnityStandardAssets.Water.MeshContainer::Update()
extern void MeshContainer_Update_mA307DB7261AAD0254EBC0013D2DB7E30C12B94CA (void);
// 0x00000075 System.Void UnityStandardAssets.Water.PlanarReflection::Start()
extern void PlanarReflection_Start_mF4B62F45289EDE1798379F46BB22090CFE446874 (void);
// 0x00000076 UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::CreateReflectionCameraFor(UnityEngine.Camera)
extern void PlanarReflection_CreateReflectionCameraFor_m536231E7168798D1B53C3C69B4C3CE24F96C1734 (void);
// 0x00000077 System.Void UnityStandardAssets.Water.PlanarReflection::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern void PlanarReflection_SetStandardCameraParameter_m18039B2FBF79EDCC215634AA15719B070D665DF9 (void);
// 0x00000078 UnityEngine.RenderTexture UnityStandardAssets.Water.PlanarReflection::CreateTextureFor(UnityEngine.Camera)
extern void PlanarReflection_CreateTextureFor_m5CC4B46CAFD4C2B62F6B2E90A3DDC566F82B73AF (void);
// 0x00000079 System.Void UnityStandardAssets.Water.PlanarReflection::RenderHelpCameras(UnityEngine.Camera)
extern void PlanarReflection_RenderHelpCameras_m8EC47193F6B3B38703D2FC5FC26A69115FB93241 (void);
// 0x0000007A System.Void UnityStandardAssets.Water.PlanarReflection::LateUpdate()
extern void PlanarReflection_LateUpdate_m67497232C28C05149AAA4EC85A3CCAB96D4F259C (void);
// 0x0000007B System.Void UnityStandardAssets.Water.PlanarReflection::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern void PlanarReflection_WaterTileBeingRendered_mAD0310016829E5BB8BAE9F0313A4654C4C7039C1 (void);
// 0x0000007C System.Void UnityStandardAssets.Water.PlanarReflection::OnEnable()
extern void PlanarReflection_OnEnable_m08B218FDAEA9D8E040927BEF9E011202783CC281 (void);
// 0x0000007D System.Void UnityStandardAssets.Water.PlanarReflection::OnDisable()
extern void PlanarReflection_OnDisable_mE5DCEA557577A4B4CD3790829AF59BE81E4CAF36 (void);
// 0x0000007E System.Void UnityStandardAssets.Water.PlanarReflection::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern void PlanarReflection_RenderReflectionFor_m743FA341870F000B1626F691C9DED6E3714D5987 (void);
// 0x0000007F System.Void UnityStandardAssets.Water.PlanarReflection::SaneCameraSettings(UnityEngine.Camera)
extern void PlanarReflection_SaneCameraSettings_m73A5C561F9B670BC714B3A4B5B6722BF8AD30C1E (void);
// 0x00000080 UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void PlanarReflection_CalculateObliqueMatrix_mFA8D1838E4D40288C73A944485471F15A9EE13AB (void);
// 0x00000081 UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void PlanarReflection_CalculateReflectionMatrix_mC0F530AB64BBB6ADCAF28DD8E014D9388CEF9271 (void);
// 0x00000082 System.Single UnityStandardAssets.Water.PlanarReflection::Sgn(System.Single)
extern void PlanarReflection_Sgn_m2C181CAD6E9DADF5B8F618D2D01649A826F8939A (void);
// 0x00000083 UnityEngine.Vector4 UnityStandardAssets.Water.PlanarReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void PlanarReflection_CameraSpacePlane_mBBA681992D91FE0286F9694E11D44BFE640A698B (void);
// 0x00000084 System.Void UnityStandardAssets.Water.PlanarReflection::.ctor()
extern void PlanarReflection__ctor_m8188E385A4C4CE7AB26873A83DB4377EA390A66F (void);
// 0x00000085 System.Void UnityStandardAssets.Water.SpecularLighting::Start()
extern void SpecularLighting_Start_mFE455D641AAD002581EA31536A30C2F01B650CEE (void);
// 0x00000086 System.Void UnityStandardAssets.Water.SpecularLighting::Update()
extern void SpecularLighting_Update_mC376C1CE2ECA4F4121AFE286F1F863EAF07720B1 (void);
// 0x00000087 System.Void UnityStandardAssets.Water.SpecularLighting::.ctor()
extern void SpecularLighting__ctor_mA0D46E411DF992BD42BCA24875516A8A5A2AAB3E (void);
// 0x00000088 System.Void UnityStandardAssets.Water.Water::OnWillRenderObject()
extern void Water_OnWillRenderObject_m030DC7B6995FEC2690883CAA454A4A72C0F5BD54 (void);
// 0x00000089 System.Void UnityStandardAssets.Water.Water::OnDisable()
extern void Water_OnDisable_mBC5924D74718A4A22BA7AFA98296F9E0247F6FFD (void);
// 0x0000008A System.Void UnityStandardAssets.Water.Water::Update()
extern void Water_Update_m67CF13DF8C862994509BF70D888E84C4CEB8BD84 (void);
// 0x0000008B System.Void UnityStandardAssets.Water.Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern void Water_UpdateCameraModes_m5A9B5A17F3C6714DC1E67305196CA9C477947011 (void);
// 0x0000008C System.Void UnityStandardAssets.Water.Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern void Water_CreateWaterObjects_m759D0CAE5A10475C04C250DF826FC862BDF6A207 (void);
// 0x0000008D UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::GetWaterMode()
extern void Water_GetWaterMode_mFF2310EBA5942F0B15B55B884F1923AF390F258D (void);
// 0x0000008E UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::FindHardwareWaterSupport()
extern void Water_FindHardwareWaterSupport_mCC07F7400748514F5A89E92835812E047D763789 (void);
// 0x0000008F UnityEngine.Vector4 UnityStandardAssets.Water.Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Water_CameraSpacePlane_mDE8B72357B143694C254E3411D4A36DB272F1CD2 (void);
// 0x00000090 System.Void UnityStandardAssets.Water.Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void Water_CalculateReflectionMatrix_mD27C10A49F7964539E94CBAC003DA65EA42003C0 (void);
// 0x00000091 System.Void UnityStandardAssets.Water.Water::.ctor()
extern void Water__ctor_mF11768BB5C88F59E159BDDA9D4713A95BCDC7EDC (void);
// 0x00000092 System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern void WaterBase_UpdateShader_mF024410B93ACDEEB86F2784367D4371789A8ECC6 (void);
// 0x00000093 System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern void WaterBase_WaterTileBeingRendered_m5AE3A512DBA0F49C5480DC7AC0832C69DA2007D6 (void);
// 0x00000094 System.Void UnityStandardAssets.Water.WaterBase::Update()
extern void WaterBase_Update_m97AB1D3EC79F89868388DEE04B9C12AC7C4F516C (void);
// 0x00000095 System.Void UnityStandardAssets.Water.WaterBase::.ctor()
extern void WaterBase__ctor_mEF380720D189D0DF1DE0F40ACB847C00D12362CC (void);
// 0x00000096 System.Void UnityStandardAssets.Water.WaterTile::Start()
extern void WaterTile_Start_m59B733629B793EA675F0934D0283148809B2D944 (void);
// 0x00000097 System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern void WaterTile_AcquireComponents_mFD2764B5DDB1CC33CA9AF55BB0D1D570EC92AD0B (void);
// 0x00000098 System.Void UnityStandardAssets.Water.WaterTile::OnWillRenderObject()
extern void WaterTile_OnWillRenderObject_m003657E9F9D7BBC9A618E204B3769E6F849C31E9 (void);
// 0x00000099 System.Void UnityStandardAssets.Water.WaterTile::.ctor()
extern void WaterTile__ctor_m5B94DD8133797CA9BF11522024360A0A1049F5E5 (void);
static Il2CppMethodPointer s_methodPointers[153] = 
{
	Box_Start_m303F32963DB378B8D1424990FB77B8667550EB60,
	Box_Update_mD1ED2D1DFF41711B93C8FF7B26229C4185747FB6,
	Box_OnTriggerStay_mBDCDFA843A559414FDEEA532C3995E6C4FE2BE5D,
	Box_OnTriggerExit_mB8E4803C99C5BA92EE0B95C6C83D6025A8E7AA8D,
	Box_blockBox_mFA226C2EFC5A3C7D8EC19E383618A6E24B363582,
	Box__ctor_mFDB4D9912FDCE47BA2A62DB77AD92F62989E0295,
	Crystals_Start_mD7F7114BF22B5D849766B4BEC0358AC581B37ABB,
	Crystals_OnTriggerEnter_m4A1C1A2B439914885D2C1DB36E373F5579833ACC,
	Crystals_CrytalColor_mC3C8AE81CE0C33215000277A927BF42DA1A75EF4,
	Crystals__ctor_m70E107C9701D646121EFF210DFA2CF12F786C92E,
	EnemyAtaque_OnTriggerEnter_m6FBE2261504A195151E794C74322FF0CD6CFCB1D,
	EnemyAtaque_StopGame_mEB738EA1A0498564C90074A2B9DD99DC71C577B7,
	EnemyAtaque__ctor_m92BC5A96F291E83BACE739D372490ACB90FE0312,
	U3CStopGameU3Ed__2__ctor_m4D499A575C66F55CD8B598DFADF2FB48B8B37D31,
	U3CStopGameU3Ed__2_System_IDisposable_Dispose_m8497D9D2FF170B8F4B29BF0B4409D33D3FAE11F9,
	U3CStopGameU3Ed__2_MoveNext_m19076221B8FB01286C8649EDBC4A044828B5A3E0,
	U3CStopGameU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04D778BC1EEC05C2DA6A3223241712CD4F8725D1,
	U3CStopGameU3Ed__2_System_Collections_IEnumerator_Reset_m09BEED7E46DAF001D5B7890A9DE5A5A90FBD100D,
	U3CStopGameU3Ed__2_System_Collections_IEnumerator_get_Current_mA84A9697F2DE0221C6DD4BC3ACE48A890E9D227C,
	EnemyMoviment_Start_m70E58D9549C8BA5938152E6294A3EBFEC5A7017D,
	EnemyMoviment_Patrol_mC320985F7F896F6E56907A42BF619D3C48F716E5,
	EnemyMoviment_OnTriggerEnter_m34E675C6A37B460217125F9384338567BFC9EBAF,
	EnemyMoviment_PauseBetweenPoints_m5A9A7B93BB7611082D8952F71FF0DDC3571B0040,
	EnemyMoviment__ctor_m8325A2F2261778D22F869016FB67A95E27489B0F,
	U3CPauseBetweenPointsU3Ed__10__ctor_mC9DFE8F507DD0C33010606A2C385414FE798DD6A,
	U3CPauseBetweenPointsU3Ed__10_System_IDisposable_Dispose_m960218053B90C48770DDC01503EEDB9FD015879D,
	U3CPauseBetweenPointsU3Ed__10_MoveNext_mCBE82EC972DF17654361740A1A5A6A479C043EAD,
	U3CPauseBetweenPointsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1251E5DA1AF26EBD3F43AC4BF23C71B1B08C879,
	U3CPauseBetweenPointsU3Ed__10_System_Collections_IEnumerator_Reset_m5D9150FCD705A629A3F83274A634DE94E36A8CDF,
	U3CPauseBetweenPointsU3Ed__10_System_Collections_IEnumerator_get_Current_m987541499C36FEC2822C0186DE09A156EF51CC12,
	AudioManager_OnEnable_m680B36494B3F0E537147056468EC66D1EFCAE97E,
	AudioManager_Start_m54C0A7ACBAB2F38052C6B900BBBC3261339662FC,
	AudioManager_PlayMusic_mB50D212EBE632BF1E95380A7F222B043A0FBFFF1,
	AudioManager_PlaySFX_m13FCA19E8F5B2A013718669280D1AABFB4767E03,
	AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD,
	GameManager_CheckScene_m5D4F9FB5CDE6F44C5078C7846C85B6C24626E652,
	GameManager_OnTriggerStay_m7CEBD224229F8B4E751CA0FB69458E32966EF75C,
	GameManager_OnTriggerExit_mCD220F68CA8D6AEDEBCAFE5603282702C9836AF4,
	GameManager_CrawlTime_m5BE2E577B1F4E6F00F9673438AE71EF756937476,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	U3CCrawlTimeU3Ed__4__ctor_mB14A3CCF90439950E906F62AC8F6297A55BDC7E9,
	U3CCrawlTimeU3Ed__4_System_IDisposable_Dispose_mAB5809BACD0B158957AE0DDA7CF1D5636D198724,
	U3CCrawlTimeU3Ed__4_MoveNext_m49379FAC79F1F1B541EDD1A44041F9E6A08B0526,
	U3CCrawlTimeU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B103A77D4FE6CEDDB1CB6D6B438E96BD396F624,
	U3CCrawlTimeU3Ed__4_System_Collections_IEnumerator_Reset_mEFE39E70A81532CBA5C3664B6EC85E3DB0324D47,
	U3CCrawlTimeU3Ed__4_System_Collections_IEnumerator_get_Current_m3C1903549C7EB63B415FB05B170CB3441AC421C3,
	MainMenu_Awake_mA8CB83E7D49CE72C6D0DE5D7D6313F8305A1A4FE,
	MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621,
	MainMenu_Update_m2DE1F2570AFBF09401821F2F89779639CD0BBC76,
	MainMenu_PlayGame_m96A3CE2743BCB00B665AA3AC575AE4EBD9ED40B0,
	MainMenu_QuitGame_m9F32E266C6F6CE345067D062258362159D267030,
	MainMenu_PauseGame_m5D489FBBACBA848C505A9431F931C16BB040A7F7,
	MainMenu_CheckPause_m54274FDDC3E0825E4BF94007091245D67C5BBD72,
	MainMenu_activeMenuGameOver_m6E930CB0636B9F8EF4D0B1844B020E26680876DC,
	MainMenu_SplashScene_m460723A3114771E99FD30F84C2BC53CAFE735FB8,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	U3CSplashSceneU3Ed__11__ctor_mBD6EB5786D88BB9EC78E63DA512AAF3C6B42CE9C,
	U3CSplashSceneU3Ed__11_System_IDisposable_Dispose_m441B5691D8AFEED842A014E366561100A8B629AA,
	U3CSplashSceneU3Ed__11_MoveNext_m1B8ED61E1DC9B157045FCBC426B3A89B6CA47821,
	U3CSplashSceneU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB8FFDE2927071EAD69DAD727F25B4B9854840CE,
	U3CSplashSceneU3Ed__11_System_Collections_IEnumerator_Reset_mFAE32AEBF64F666CA4DC59D60EC978554E413667,
	U3CSplashSceneU3Ed__11_System_Collections_IEnumerator_get_Current_mA57B369812F6C456C250B031ED907F1BCAE3BA9B,
	ScoreManager_Awake_mB6C30C958421EED1082D2D5B24532F2548DB4575,
	ScoreManager_GetPoints_m51748B35A7A793CC843753B2A90B6DA6EBC77026,
	ScoreManager_UpdateScoreUI_mF6CBF4DD6A6228B7F2CA86F58C4BBB491EA3D3C6,
	ScoreManager__ctor_m638A240D34643E8AB9D17553622C1C9354348354,
	UIManager_Awake_mCED93604270B1E209B4E0D32F6A26DDC5AB06E30,
	UIManager_changeText_mD82273B90CC2E903B4A24BF5B6F4E3D4BC63AD69,
	UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B,
	CamSceneOne_LateUpdate_mB71C3FD11B2E30C2D62BF1247947E9716BAC9A55,
	CamSceneOne_cameraMovement_m1EB774E19A4EE7D75B8AC11E6A8AEF8C6C052311,
	CamSceneOne__ctor_m99070A08F49CC7CAF0DD20592E652B13640CFA29,
	CamSceneTwo_Start_mF65C09486D0A4E0EEF3BB1AFAB1297B1FD1AADE0,
	CamSceneTwo_FixedUpdate_mED03D1FD1FC64EE9ACFBBF519332E9F0D03EB100,
	CamSceneTwo_follow_m857882F07ED1FA7B49A979D36C77590C9EB9D6FE,
	CamSceneTwo__ctor_m0386A176B970A9F32859B4A7599EC6E09DD81C59,
	Movement_Start_m5FA5146A031A9B13FE98F9CCD6027EB1DBA2DF4F,
	Movement_Update_m0880BACB69D5C89071A82EAB9BC17F76151B7DF1,
	Movement_MovementPlayer_m01B9AFE57EDEE1995CA1E35EE9D9DC554E36B4D8,
	Movement_OnTriggerStay_mA2C4F3EACDECE088DF2D148E58C9E393A1388B6E,
	Movement_OnTriggerExit_m4E66F24F5C693037E14252BAB22EE3F69BEF3C41,
	Movement_Animations_mEA87C9BA145FDF2E848EE68804BE5DBDF5404A98,
	Movement_endJump_m7421F9EABF0E6675E6718831C80E805B811707FE,
	Movement_Idle_m9F3C6CDB22589BE562FA30C205D8D8E7950DB8C7,
	Movement_Run_m7FE2E54C885DA5DED13A3105881E78C6DA3DD390,
	Movement_WalkBack_mF171B314D5575D2EE779CECCC34E86AD4A64B9D1,
	Movement_Jump_mD745E7B705D37BC4F7CEF0C2A4575E5DB15EBE1C,
	Movement_Pull_m566BA1CF89DA78074414178B601F7C6A215F852C,
	Movement_Push_m3A0F2220CEE2742AE70995AC06F244CBA7054384,
	Movement_Crawl_m16B950AAD76EEE17EB1D6148852988C1F3E53375,
	Movement_Fall_m2C979047F3A8AD028694AE7D7E84374F39EF7E99,
	Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB,
	U3CendJumpU3Ed__16__ctor_m76B02F86BA012943C65ECD3DD1967DDE1DFC4E1C,
	U3CendJumpU3Ed__16_System_IDisposable_Dispose_m90A5ADEB46FBA2B51FE8323140BFCFBC3C152DDB,
	U3CendJumpU3Ed__16_MoveNext_m7773C3A634586FFB8C818E3A8BE57DD63AD4E737,
	U3CendJumpU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA71A8227E7DCF926A952C7580A097102F54F62CF,
	U3CendJumpU3Ed__16_System_Collections_IEnumerator_Reset_m1A02DB8D86D328ED4A27C6CA5082EE8E5D8653F3,
	U3CendJumpU3Ed__16_System_Collections_IEnumerator_get_Current_m884936D4F2743C7088AABED8567D6E3FB79EEB0F,
	PlayerMove_Awake_mD0B694351FB695C65CAFAD8C097849AF11391C4A,
	PlayerMove_Start_m472BE983E7DC91CA752BB2994C83384C9F0DB277,
	PlayerMove_Update_m9AC3A24B5AFBD10381EFFC5A68D0DB56C73E2D90,
	PlayerMove_Move_m4614EC8657FCE8F2C9D5657085B96830F20DD306,
	PlayerMove__ctor_mDCA304173F32D13822B1DF8FA9F46A4446A215D9,
	PullingBox_Awake_m2AF238B3DCB605107C52FD58D2049F54B19D235D,
	PullingBox_OnTriggerStay_m8C7BE204DD4B80A5069E2EEE10ECEFB2D0F018CB,
	PullingBox_OnTriggerExit_mE2A01C0C1097D2E6A94D4AD93E9A70D6D219F8AB,
	PullingBox__ctor_mF036F4DD83CC24562C4099B5D15CCA987C1EC23B,
	WaterBasic_Update_m79AE15CDF47AA396823D10220EBA44C34CD6F569,
	WaterBasic__ctor_mF258EB45327E0929D2CB72B5266080C3F2E9CF4A,
	Displace_Awake_mE99DEB129DED3AF1991C408D98A52E342B4643A7,
	Displace_OnEnable_m952A8DDD7BF19B262520923CACCAE21927044BD8,
	Displace_OnDisable_m344E8D36AD672D92E465CD50F28E512A817B8B08,
	Displace__ctor_mA627066AA526C79EF3AA8D4FCDF70261CEA5CFA8,
	GerstnerDisplace__ctor_m718281CC56902E6BF82FAC236AB3043BFA0E9BFF,
	MeshContainer__ctor_mF1E5D7C018B2F9F756CECB8C1A0497406F833E3B,
	MeshContainer_Update_mA307DB7261AAD0254EBC0013D2DB7E30C12B94CA,
	PlanarReflection_Start_mF4B62F45289EDE1798379F46BB22090CFE446874,
	PlanarReflection_CreateReflectionCameraFor_m536231E7168798D1B53C3C69B4C3CE24F96C1734,
	PlanarReflection_SetStandardCameraParameter_m18039B2FBF79EDCC215634AA15719B070D665DF9,
	PlanarReflection_CreateTextureFor_m5CC4B46CAFD4C2B62F6B2E90A3DDC566F82B73AF,
	PlanarReflection_RenderHelpCameras_m8EC47193F6B3B38703D2FC5FC26A69115FB93241,
	PlanarReflection_LateUpdate_m67497232C28C05149AAA4EC85A3CCAB96D4F259C,
	PlanarReflection_WaterTileBeingRendered_mAD0310016829E5BB8BAE9F0313A4654C4C7039C1,
	PlanarReflection_OnEnable_m08B218FDAEA9D8E040927BEF9E011202783CC281,
	PlanarReflection_OnDisable_mE5DCEA557577A4B4CD3790829AF59BE81E4CAF36,
	PlanarReflection_RenderReflectionFor_m743FA341870F000B1626F691C9DED6E3714D5987,
	PlanarReflection_SaneCameraSettings_m73A5C561F9B670BC714B3A4B5B6722BF8AD30C1E,
	PlanarReflection_CalculateObliqueMatrix_mFA8D1838E4D40288C73A944485471F15A9EE13AB,
	PlanarReflection_CalculateReflectionMatrix_mC0F530AB64BBB6ADCAF28DD8E014D9388CEF9271,
	PlanarReflection_Sgn_m2C181CAD6E9DADF5B8F618D2D01649A826F8939A,
	PlanarReflection_CameraSpacePlane_mBBA681992D91FE0286F9694E11D44BFE640A698B,
	PlanarReflection__ctor_m8188E385A4C4CE7AB26873A83DB4377EA390A66F,
	SpecularLighting_Start_mFE455D641AAD002581EA31536A30C2F01B650CEE,
	SpecularLighting_Update_mC376C1CE2ECA4F4121AFE286F1F863EAF07720B1,
	SpecularLighting__ctor_mA0D46E411DF992BD42BCA24875516A8A5A2AAB3E,
	Water_OnWillRenderObject_m030DC7B6995FEC2690883CAA454A4A72C0F5BD54,
	Water_OnDisable_mBC5924D74718A4A22BA7AFA98296F9E0247F6FFD,
	Water_Update_m67CF13DF8C862994509BF70D888E84C4CEB8BD84,
	Water_UpdateCameraModes_m5A9B5A17F3C6714DC1E67305196CA9C477947011,
	Water_CreateWaterObjects_m759D0CAE5A10475C04C250DF826FC862BDF6A207,
	Water_GetWaterMode_mFF2310EBA5942F0B15B55B884F1923AF390F258D,
	Water_FindHardwareWaterSupport_mCC07F7400748514F5A89E92835812E047D763789,
	Water_CameraSpacePlane_mDE8B72357B143694C254E3411D4A36DB272F1CD2,
	Water_CalculateReflectionMatrix_mD27C10A49F7964539E94CBAC003DA65EA42003C0,
	Water__ctor_mF11768BB5C88F59E159BDDA9D4713A95BCDC7EDC,
	WaterBase_UpdateShader_mF024410B93ACDEEB86F2784367D4371789A8ECC6,
	WaterBase_WaterTileBeingRendered_m5AE3A512DBA0F49C5480DC7AC0832C69DA2007D6,
	WaterBase_Update_m97AB1D3EC79F89868388DEE04B9C12AC7C4F516C,
	WaterBase__ctor_mEF380720D189D0DF1DE0F40ACB847C00D12362CC,
	WaterTile_Start_m59B733629B793EA675F0934D0283148809B2D944,
	WaterTile_AcquireComponents_mFD2764B5DDB1CC33CA9AF55BB0D1D570EC92AD0B,
	WaterTile_OnWillRenderObject_m003657E9F9D7BBC9A618E204B3769E6F849C31E9,
	WaterTile__ctor_m5B94DD8133797CA9BF11522024360A0A1049F5E5,
};
static const int32_t s_InvokerIndices[153] = 
{
	3333,
	3333,
	2668,
	2668,
	2688,
	3333,
	3333,
	2668,
	3333,
	3333,
	2668,
	3280,
	3333,
	2652,
	3333,
	3303,
	3280,
	3333,
	3280,
	3333,
	3333,
	2668,
	2047,
	3333,
	2652,
	3333,
	3303,
	3280,
	3333,
	3280,
	3333,
	3333,
	2668,
	2668,
	3333,
	3333,
	2668,
	2668,
	3280,
	3333,
	2652,
	3333,
	3303,
	3280,
	3333,
	3280,
	3333,
	3333,
	3333,
	3333,
	3333,
	2688,
	3333,
	3333,
	3280,
	3333,
	2652,
	3333,
	3303,
	3280,
	3333,
	3280,
	3333,
	2652,
	3333,
	3333,
	3333,
	2668,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	2668,
	2668,
	3333,
	3280,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	2652,
	3333,
	3303,
	3280,
	3333,
	3280,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	2668,
	2668,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	2668,
	3333,
	3333,
	2054,
	1475,
	2054,
	2668,
	3333,
	1476,
	3333,
	3333,
	1476,
	2668,
	4399,
	4399,
	4939,
	496,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
	1476,
	846,
	3263,
	3263,
	496,
	4616,
	3333,
	3333,
	1476,
	3333,
	3333,
	3333,
	3333,
	3333,
	3333,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	153,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
